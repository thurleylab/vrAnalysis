import os
import h5py
import numpy as np

import matplotlib
#matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.pyplot import figure
from analysis.plots.placecells.draw import draw_place_firing
from mpl_toolkits.axes_grid1 import make_axes_locatable
from analysis.plots.placecells.utils import get_figure_size
from analysis.processing.epochs import get_epochs

COLORS = ['#336688', '#D95D39', '#74A57F', '#E3B505', '#E7ECEF']


def common_epoch_figure(session_path, is_boot=False, filename_postfix='', exclude_epochs=(), verbose=True):

    def spiking_plot(pos, traj_idxs, spk_idxs, show_y_ticks=False):
        axPosA = fig.add_subplot(pos)
        axPosA.set_xlim(limits[0], limits[1])
        axPosA.set_ylim(limits[2], limits[3])
        axPosA.set_xticks([])
        if not show_y_ticks:
            axPosA.set_yticklabels([])
        axPosA.scatter(trajectory[traj_idxs][:, 1], trajectory[traj_idxs][:, 2], alpha=0.05, color='#B4B8AB')
        axPosA.scatter(trajectory[spk_idxs][:, 1], trajectory[spk_idxs][:, 2], alpha=0.5, color='#336688')  # '#9b1d20'
        axPosA.set_aspect('equal')
        axPosA.grid(axis='y')
        return axPosA

    def field_plot(pos, pf, inf, spa, sel, sic, coh, mfr, bur, iso, vmax, show_y_ticks=False):
        axA = fig.add_subplot(pos)
        axA.set_xticks([])
        if not show_y_ticks:
            axA.set_yticklabels([])
        axA.grid(axis='y')
        axA.set_xlim(limits[0], limits[1])
        axA.set_ylim(limits[2], limits[3])
        posPFA = axA.imshow(pf.T, cmap='jet', origin='lower', extent=extent, vmax=vmax)
        divider = make_axes_locatable(axA)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        fig.colorbar(posPFA, cax=cax)
        textstr = '\n'.join((
            #'inf=%.2f' % (inf,),
            #'spa=%.2f' % (spa,),
            #'sel=%.2f' % (sel,),
            'sic=%.2f' % (sic,),
            #'coh=%.2f' % (coh,),
            'mfr=%.2f' % (mfr,),
            #'bur=%.2f' % (bur,),
            #'iso=%.2f' % (iso,)
        ))
        props = dict(boxstyle='round', facecolor='white', alpha=0.5)
        axA.text(0.05, 0.95, textstr, transform=axA.transAxes, fontsize=14,
                 verticalalignment='top', bbox=props)
        return axA

    def patches_plot(pos, fields, COM):
        axFieldsA = fig.add_subplot(pos)
        axFieldsA.grid(axis='y')
        axFieldsA.set_xticks([])
        if not show_y_ticks:
            axFieldsA.set_yticklabels([])
        axFieldsA.set_xlim(limits[0], limits[1])
        axFieldsA.set_ylim(limits[2], limits[3])

        if COM is None:
            return axFieldsA

        # Fields
        field_ids = COM[:2][:, 0]     # first two field IDs
        filt_fields = fields.T
        for f_i, field_id in enumerate(field_ids):
            filt_fields[filt_fields == field_id] = 10  # highlight first 2 fields (best ones)
        axFieldsA.imshow(filt_fields, cmap='GnBu', origin='lower', extent=extent, interpolation='nearest')

        # COMs
        axFieldsA.scatter(COM[:2][:, 1], COM[:2][:, 2], s=100, marker="o", color="black")
        for irow, row in enumerate(COM[:2]):
            f_id = row[0]
            f_peak_rate = row[3]
            txt = "%d (%.1f)" % (f_id, f_peak_rate)
            axFieldsA.annotate(txt, (COM[:2][:, 1][irow] + 0.05, COM[:2][:, 2][irow] + 0.05), fontsize=14)
        return axFieldsA

    s_path = session_path[:-1] if session_path.endswith('/') else session_path
    animal = s_path.split('/')[-2]
    session_id = s_path.split('/')[-1]

    # sorting for proper display
    conditions = list(get_epochs(session_path).keys())
    # remove excluded
    conditions = list(set(conditions) - set(exclude_epochs))

    if 'Ad' or 'Bd' in conditions:
        func1 = lambda x: 'z' + x if x.find('d') > -1 else x
        func2 = lambda x: x[1:] if x.find('z') > -1 else x

        tmp = [func1(x) for x in conditions]
        tmp.sort()
        conditions = [func2(x) for x in tmp]

    else:
        conditions.sort()

    # reading data
    h5file = os.path.join(session_path, 'all.h5')
    h5_fields_file = os.path.join(session_path, 'bootstrap.h5') if is_boot else h5file

    with h5py.File(h5file, 'r') as f:
        trajectory = np.array(f['animal_trajectory'])
        unit_groups = [x for x in f['units']]

    # processing single units
    for unit_group_name in unit_groups:
        electrode = unit_group_name.split('_')[0]
        unit = unit_group_name.split('_')[1]
        axes_collection = []
        COM_collection = []

        spec2 = gridspec.GridSpec(ncols=len(conditions), nrows=3)
        fig = figure(figsize=(3 * len(conditions), 5 * 3))
        limits = get_figure_size(trajectory[:, 1:3], border=0.1)

        # first compute maximum peak firing rate to normalize field plots
        fr_all = []
        for j, cond in enumerate(conditions):
            with h5py.File(h5file, 'r') as f:
                unit_group = f['units']['%s_%s' % (electrode, unit)]
                fr_all.append(np.array(unit_group[cond + '_place_field']).max())

        for j, cond in enumerate(conditions):

            # ------------- reading unit data ---------------

            with h5py.File(h5file, 'r') as f:
                unit_group = f['units']['%s_%s' % (electrode, unit)]

                # Unit firing
                pos_firing = np.array(unit_group['trajectory_indexes'])
                traj_indexes = np.array(f[cond + '_indexes'])
                pf = np.array(unit_group[cond + '_place_field'])

                # Unit metrics
                mfr = unit_group.attrs['mean_firing_rate'][0]
                bur = unit_group.attrs['burst_index'][0]
                iso = unit_group.attrs['isolation_distance'][0]

                # Field metrics
                inf = unit_group[cond + '_place_field'].attrs['information']  # by Christian / Dustin
                spa = unit_group[cond + '_place_field'].attrs['sparsity']
                sel = unit_group[cond + '_place_field'].attrs['selectivity']
                sic = unit_group[cond + '_place_field'].attrs['information_content']  # from Resnik (Save et al., 2000; Markus et al.,1994; Skaggs et al., 1993).
                coh = unit_group[cond + '_place_field'].attrs['spatial_coherence']  # from Resnik (Rotenberg et al., 1996; Save et al., 2000)
                extent = np.array(unit_group.attrs['xy_range'])

            with h5py.File(h5_fields_file, 'r') as f:
                if is_boot:
                    cond_group = f['%s_%s' % (electrode, unit)][cond]
                    fields = np.array(cond_group['fields']) if 'fields' in cond_group else np.zeros(pf.shape)
                    COMs = np.array(cond_group['clusters']) if 'clusters' in cond_group else None
                else:
                    unit_group = f['units']['%s_%s' % (electrode, unit)]
                    fields = np.array(unit_group[cond + '_fields'])
                    COMs = np.array(unit_group[cond + '_fields_COMs']) if cond + '_fields_COMs' in unit_group else None

            # ------------- plots ---------------

            vmax = np.array(fr_all).max()
            show_y_ticks = j == 0  # show ticks only in the first column

            # spiking + trajectories
            ax1 = spiking_plot(spec2[0, j], traj_indexes, np.intersect1d(pos_firing, traj_indexes), show_y_ticks)
            ax1.set_title(cond, fontsize=20)

            # place fields
            ax2 = field_plot(spec2[1, j], pf, inf, spa, sel, sic, coh, mfr, bur, iso, vmax, show_y_ticks)

            # selected field patches
            if COMs is not None:
                filter_by = 7 if is_boot else 3
                sort_idxs = np.argsort(COMs[:, filter_by])[::-1]  # sort by number of points in the cluster
                ax3 = patches_plot(spec2[2, j], fields, COMs[sort_idxs])
            else:
                ax3 = patches_plot(spec2[2, j], fields, COMs)

            axes_collection.append([ax1, ax2, ax3])
            COM_collection.append(COMs)

        # ------------- saving to .PNG ---------------
        save_to = 'bootstrapped' if is_boot else 'placefields'

        if not os.path.exists(os.path.join(session_path, 'analysis')):
            os.makedirs(os.path.join(session_path, 'analysis'))
        if not os.path.exists(os.path.join(session_path, 'analysis', save_to)):
            os.makedirs(os.path.join(session_path, 'analysis', save_to))

        fig.tight_layout()

        filename = "%s_%s_%s_%s%s.png" % (animal, session_id, electrode, unit, filename_postfix)
        fig.savefig(os.path.join(os.path.join(session_path, 'analysis', save_to), filename))

        plt.close(fig)
        plt.clf()

        print("%s: el %s unit %s figure created" % (session_id, electrode, unit))

    # print("\rProcessed: ", session_id, round(100. * float(i+1) / total, 2), "%", "%s/%s" % (i+1, total), end="")
    # print('Session %s done (%s of %s)' % (session_id, i + 1, len(sessions)))
