import numpy as np


def get_figure_size(arr, border=0.1):
    x_min = arr[:, 0].min()
    x_max = arr[:, 0].max()
    y_min = arr[:, 1].min()
    y_max = arr[:, 1].max()

    x_diff = abs(x_min - x_max)
    y_diff = abs(y_min - y_max)

    x_mid = x_max - x_diff / 2
    y_mid = y_max - y_diff / 2

    # x_dist = (1 + 2 * border) * (x_diff / 2)
    # y_dist = (1 + 2 * border) * (y_diff / 2)

    x_dist = border + x_diff / 2
    y_dist = border + y_diff / 2

    #return x_mid - dist, x_mid + dist, y_mid - dist, y_mid + dist
    return x_mid - x_dist, x_mid + x_dist, y_mid - y_dist, y_mid + y_dist


def get_extent(all_pos):
    return [all_pos[:, 0].min(), all_pos[:, 0].max(), all_pos[:, 1].min(), all_pos[:, 1].max()]


def get_data_for_fields(session, ttd, unit, an_traj_0, an_traj_1, an_traj_0_idxs, an_traj_1_idxs, sampling_rate=50):
    """

    :param ttd:
    :param unit:
    :param sampling_rate:
    :return:
    """
    # positions when unit fires for A + 0 and A + 30
    unit_fire_0 = session.get_positions_for_unit(ttd, unit, an_traj_0_idxs, refresh=True)
    unit_fire_1 = session.get_positions_for_unit(ttd, unit, an_traj_1_idxs, refresh=True)

    # all positions for A + 0
    pos_at_f0 = [an_traj_0[0]]
    for pos in an_traj_0:
        if pos[0] - pos_at_f0[-1][0] > (1 / float(sampling_rate)):
            pos_at_f0.append(pos)

    pos_at_f0 = np.array(pos_at_f0)

    # all positions for A + 30
    pos_at_f1 = [an_traj_1[0]]
    for pos in an_traj_1:
        if pos[0] - pos_at_f1[-1][0] > (1 / float(sampling_rate)):
            pos_at_f1.append(pos)

    pos_at_f1 = np.array(pos_at_f1)

    return unit_fire_0, unit_fire_1, pos_at_f0, pos_at_f1
