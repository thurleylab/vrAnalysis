from .utils import get_figure_size, get_extent
from .draw import draw_occupancy, draw_autocorrelogram, draw_place_field
from analysis.models.analytics import place_field_2D
from scipy.signal import correlate2d
from matplotlib.pyplot import figure

import os
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np


def all_AB_cells(session, an_traj_0_idxs, an_traj_1_idxs):
    counter = 0

    units_total = len([unit for ttd in session.tetrode_unit_map.values() for unit in ttd])

    ax_x_axes = 3
    ax_y_axes = int(np.ceil(float(units_total) / ax_x_axes))

    fig = figure(figsize=(4 * ax_x_axes, 4 * ax_y_axes))

    limits = get_figure_size(session.trajectory[:, 1:3])
    trajectory = session.trajectory

    for ttd, units in session.tetrode_unit_map.items():
        if len(units) > 0:
            print('\rProcessing unit group %s..' % str(ttd))  # , end=""

        for unit in units:
            counter += 1

            unit_fire_0 = session.get_positions_for_unit2(ttd, unit, an_traj_0_idxs)
            unit_fire_1 = session.get_positions_for_unit2(ttd, unit, an_traj_1_idxs)

            # firing Arena + 0
            ax = fig.add_subplot(ax_y_axes, ax_x_axes, counter)
            draw_occupancy(ax, trajectory[:, 1:3], limits=limits)

            unit_pos_0 = unit_fire_0[:, 1:3]
            unit_pos_1 = unit_fire_1[:, 1:3]

            ax.scatter(unit_pos_0[:, 0], unit_pos_0[:, 1], alpha=0.5, color='#e98a15')
            ax.scatter(unit_pos_1[:, 0], unit_pos_1[:, 1], alpha=0.5, color='#89043d')

            ax.set_title("Electrode: %s, Unit: %s" % (ttd, unit), fontsize=14)

    fig.tight_layout()
    return fig


def all_ABC_cells(session, an_traj_0_idxs, an_traj_1_idxs, an_traj_2_idxs):
    counter = 0

    units_total = len([unit for ttd in session.tetrode_unit_map.values() for unit in ttd])

    ax_x_axes = 3
    ax_y_axes = int(np.ceil(float(units_total) / ax_x_axes))

    fig = figure(figsize=(4 * ax_x_axes, 4 * ax_y_axes))

    limits = get_figure_size(session.trajectory[:, 1:3])
    trajectory = session.trajectory

    for ttd, units in session.tetrode_unit_map.items():
        if len(units) > 0:
            print('\rProcessing unit group %s..' % str(ttd))  # , end=""

        for unit in units:
            counter += 1

            unit_fire_0 = session.get_positions_for_unit2(ttd, unit, an_traj_0_idxs)
            unit_fire_1 = session.get_positions_for_unit2(ttd, unit, an_traj_1_idxs)
            unit_fire_2 = session.get_positions_for_unit2(ttd, unit, an_traj_2_idxs)

            # firing Arena + 0
            ax = fig.add_subplot(ax_y_axes, ax_x_axes, counter)
            draw_occupancy(ax, trajectory[:, 1:3], limits=limits)

            # unit_pos_0 = unit_fire_0[:, 1:3]
            # unit_pos_1 = unit_fire_1[:, 1:3]
            # unit_pos_2 = unit_fire_2[:, 1:3]

            if len(unit_fire_0) > 0:
                ax.scatter(unit_fire_0[:, 1], unit_fire_0[:, 2], alpha=0.5, color='#e98a15')

            if len(unit_fire_1) > 0:
                ax.scatter(unit_fire_1[:, 1], unit_fire_1[:, 2], alpha=0.5, color='#89043d')

            if len(unit_fire_2) > 0:
                ax.scatter(unit_fire_2[:, 1], unit_fire_2[:, 2], alpha=0.5, color='#003b36')

            ax.set_title("Electrode: %s, Unit: %s" % (ttd, unit), fontsize=14)

    fig.tight_layout()
    return fig


def shift_plot(session, ttd_map, an_traj_0, an_traj_1, an_traj_0_idxs, an_traj_1_idxs):
    units_total = len([unit for ttd in ttd_map.values() for unit in ttd])
    fig = figure(figsize=(4 * 4, 6 * units_total))

    limits = get_figure_size(session.trajectory[:, 1:3])
    trajectory = session.trajectory

    counter = 0

    for ttd, units in ttd_map.items():
        for unit in units:
            unit_fire_0 = session.get_positions_for_unit(ttd, unit, an_traj_0_idxs, refresh=True)
            unit_fire_1 = session.get_positions_for_unit(ttd, unit, an_traj_1_idxs, refresh=True)

            # autocorr
            ax = fig.add_subplot(units_total, 4, 4 * counter + 1)
            draw_autocorrelogram(ax, session.get_autocorr(ttd, unit), color='#003b36')
            ax.set_title("Electrode: %s, Unit: %s" % (ttd, unit), fontsize=14)

            # firing Arena + 0
            ax = fig.add_subplot(units_total, 4, 4 * counter + 2)
            draw_occupancy(ax, an_traj_0[:, 1:3], limits=limits)
            ax.scatter(unit_fire_0[:, 1:3][:, 0], unit_fire_0[:, 1:3][:, 1], alpha=0.5, color='#e98a15')

            # firing Arena + 30
            ax = fig.add_subplot(units_total, 4, 4 * counter + 3)
            draw_occupancy(ax, an_traj_1[:, 1:3], limits=limits)
            ax.scatter(unit_fire_1[:, 1:3][:, 0], unit_fire_1[:, 1:3][:, 1], alpha=0.5, color='#89043d')

            # firing Arena ALL
            ax = fig.add_subplot(units_total, 4, 4 * counter + 4)
            draw_occupancy(ax, trajectory[:, 1:3], limits=limits)

            unit_pos_0 = unit_fire_0[:, 1:3]
            unit_pos_1 = unit_fire_1[:, 1:3]

            ax.scatter(unit_pos_0[:, 0], unit_pos_0[:, 1], alpha=0.5, color='#e98a15')
            ax.scatter(unit_pos_1[:, 0], unit_pos_1[:, 1], alpha=0.5, color='#89043d')

            counter += 1

    return fig


def shift_4_plot(session, ttd_map, an_traj_0, an_traj_1, an_traj_0_idxs, an_traj_1_idxs):
    units_total = len([unit for ttd in ttd_map.values() for unit in ttd])
    fig = figure(figsize=(4 * 4, 6 * units_total))

    limits = get_figure_size(session.trajectory[:, 1:3])
    trajectory = session.trajectory

    counter = 0

    for ttd, units in ttd_map.items():
        for unit in units:
            unit_fire_0 = session.get_positions_for_unit(ttd, unit, an_traj_0_idxs, refresh=True)
            unit_fire_1 = session.get_positions_for_unit(ttd, unit, an_traj_1_idxs, refresh=True)

            # autocorr
            ax = fig.add_subplot(units_total, 4, 4 * counter + 1)
            draw_autocorrelogram(ax, session.get_autocorr(ttd, unit), color='#003b36')
            ax.set_title("Electrode: %s, Unit: %s" % (ttd, unit), fontsize=14)

            # firing Arena + 0
            ax = fig.add_subplot(units_total, 4, 4 * counter + 2)
            draw_occupancy(ax, an_traj_0[:, 1:3], limits=limits)
            ax.scatter(unit_fire_0[:, 1:3][:, 0], unit_fire_0[:, 1:3][:, 1], alpha=0.5, color='#e98a15')

            # firing Arena + 30
            ax = fig.add_subplot(units_total, 4, 4 * counter + 3)
            draw_occupancy(ax, an_traj_1[:, 1:3], limits=limits)
            ax.scatter(unit_fire_1[:, 1:3][:, 0], unit_fire_1[:, 1:3][:, 1], alpha=0.5, color='#89043d')

            # firing Arena ALL
            ax = fig.add_subplot(units_total, 4, 4 * counter + 4)
            draw_occupancy(ax, trajectory[:, 1:3], limits=limits)

            unit_pos_0 = unit_fire_0[:, 1:3]
            unit_pos_1 = unit_fire_1[:, 1:3]

            ax.scatter(unit_pos_0[:, 0], unit_pos_0[:, 1], alpha=0.5, color='#e98a15')
            ax.scatter(unit_pos_1[:, 0], unit_pos_1[:, 1], alpha=0.5, color='#89043d')

            counter += 1

    return fig


def single_unit_plot(session, ttd, unit, unit_fire_0, unit_fire_1, pos_at_f0, pos_at_f1, an_traj_0, an_traj_1):
    fig = figure(figsize=(12, 16))

    gs = gridspec.GridSpec(4, 3)
    ax1 = plt.subplot(gs[0:2, 0:2])  # big plot
    ax2 = plt.subplot(gs[0, 2])  # autocorr
    ax3 = plt.subplot(gs[1, 2])  # occupancy
    ax4 = plt.subplot(gs[2, 0])  # firing Arena + 0
    ax5 = plt.subplot(gs[2, 1])  # firing Arena + 30
    ax6 = plt.subplot(gs[2, 2])  # firing overlapping
    ax7 = plt.subplot(gs[3, 0])  # field Arena + 0
    ax8 = plt.subplot(gs[3, 1])  # field Arena + 30
    ax9 = plt.subplot(gs[3, 2])  # cross-corr

    sampling_rate = 50

    # big plot
    limits = get_figure_size(session.trajectory[:, 1:3])

    img = plt.imread(os.path.join(session.path, "arena.png"))

    ax1.imshow(img, extent=[-1.05, 0.7, -1.2, 1.5])

    draw_occupancy(ax1, session.trajectory[:, 1:3], size=10, limits=limits)

    unit_pos_0 = unit_fire_0[:, 1:3]
    unit_pos_1 = unit_fire_1[:, 1:3]

    ax1.scatter(unit_pos_0[:, 0], unit_pos_0[:, 1], s=20, alpha=0.5, color='#e98a15')  # 003b36
    ax1.scatter(unit_pos_1[:, 0], unit_pos_1[:, 1], s=20, alpha=0.5, color='#89043d')
    ax1.set_title("Electrode: %s, Unit: %s" % (ttd, unit), fontsize=14)

    # autocorr
    draw_autocorrelogram(ax2, session.get_autocorr(ttd, unit))

    # occupancy
    draw_occupancy(ax3, session.trajectory[:, 1:3], size=5, limits=limits)

    # firing Arena + 0
    draw_occupancy(ax4, an_traj_0[:, 1:3], size=5, limits=limits)
    ax4.scatter(unit_fire_0[:, 1:3][:, 0], unit_fire_0[:, 1:3][:, 1], s=10, alpha=0.5, color='#e98a15')

    # firing Arena + 30
    draw_occupancy(ax5, an_traj_1[:, 1:3], size=5, limits=limits)
    ax5.scatter(unit_fire_1[:, 1:3][:, 0], unit_fire_1[:, 1:3][:, 1], s=10, alpha=0.5, color='#89043d')

    # firing overlapping
    draw_occupancy(ax6, session.trajectory[:, 1:3], size=5, limits=limits)
    ax6.scatter(unit_pos_0[:, 0], unit_pos_0[:, 1], s=10, alpha=0.5, color='#e98a15')  # 003b36
    ax6.scatter(unit_pos_1[:, 0], unit_pos_1[:, 1], s=10, alpha=0.5, color='#89043d')

    # field Arena + 0
    extent = get_extent(pos_at_f0[:, 1:3])
    omap, smap, fmap, s_firing_map_0 = place_field_2D(pos_at_f0[:, 1:3], unit_fire_0[:, 1:3], sampling_rate, 0.03)
    draw_place_field(ax7, s_firing_map_0, limits, extent=extent)

    # field Arena + 30
    extent = get_extent(pos_at_f1[:, 1:3])
    omap, smap, fmap, s_firing_map_1 = place_field_2D(pos_at_f1[:, 1:3], unit_fire_1[:, 1:3], sampling_rate, 0.03)
    draw_place_field(ax8, s_firing_map_1, limits, extent=extent)

    # cross-corr
    ccg = correlate2d(s_firing_map_0, s_firing_map_1, mode='same')
    extent = get_extent(session.trajectory[:, 1:3])
    draw_place_field(ax9, ccg.T, limits=limits, extent=extent)

    return fig


def gain_comparison_plot(session, ttd_map, an_traj_0, an_traj_1, an_traj_0_idxs, an_traj_1_idxs):
    units_total = len([unit for ttd in ttd_map.values() for unit in ttd])
    fig = figure(figsize=(4 * 4, 2 * 6 * (units_total)))

    limits = get_figure_size(session.trajectory[:, 1:3], border=0.3)
    extent = np.array(get_extent(session.trajectory[:, 1:3]))
    trajectory = session.trajectory

    # defining center of trajectories
    x = (an_traj_1[:, 1].min() + an_traj_1[:, 1].max()) / 2
    y = (an_traj_1[:, 2].min() + an_traj_1[:, 2].max()) / 2

    counter = 0
    sampling_rate = 50

    def inflate(traj, gain=1.5):
        res = np.array(traj)

        res[:, 0] -= x
        res[:, 0] = res[:, 0] * gain
        res[:, 0] += x

        res[:, 1] -= y
        res[:, 1] = res[:, 1] * gain
        res[:, 1] += y

        return res

    def draw_occupancy(ax, traj):
        ax.grid(True)
        ax.set_xlabel('X, m')
        ax.set_xlim(limits[0], limits[1])
        ax.set_ylim(limits[2], limits[3])
        ax.scatter(traj[:, 1], traj[:, 2], s=10, alpha=0.05, color='#acbea3')

    def downsample(traj):
        pos_at_f = [traj[0]]
        for pos in traj:
            if pos[0] - pos_at_f[-1][0] > (1 / float(sampling_rate)):
                pos_at_f.append(pos)

        return np.array(pos_at_f)

    for ttd, units in ttd_map.items():
        for unit in units:
            unit_fire_0 = session.get_positions_for_unit2(ttd, unit, an_traj_0_idxs)
            unit_fire_1 = session.get_positions_for_unit2(ttd, unit, an_traj_1_idxs)

            pos_at_f0 = downsample(an_traj_0)
            pos_at_f1 = downsample(an_traj_1)

            # NO GAIN session remains unchanged
            unit_pos_0 = unit_fire_0[:, 1:3]

            # GAIN session is inflated 1.5 times
            unit_pos_1 = inflate(unit_fire_1[:, 1:3])

            extent_i = get_extent(inflate(pos_at_f1[:, 1:3]))
            lgth_x = limits[1] - limits[0]
            lgth_y = limits[3] - limits[2]

            # 1 - firing no gain
            ax = fig.add_subplot(2 * units_total, 3, 6 * counter + 1)
            draw_occupancy(ax, an_traj_0)
            ax.set_ylabel('%s / %s, Y, m' % (ttd, unit))
            ax.scatter(unit_fire_0[:, 1], unit_fire_0[:, 2], alpha=0.3, color='#e98a15')

            # 2 - firing with gain
            ax = fig.add_subplot(2 * units_total, 3, 6 * counter + 2)
            draw_occupancy(ax, an_traj_1)
            ax.scatter(unit_fire_1[:, 1], unit_fire_1[:, 2], alpha=0.3, color='#89043d')
            # ax.scatter(unit_pos_1[:, 0], unit_pos_1[:, 1], alpha=0.3, color='#89043d')

            # 3 - firing Arena GAIN:
            ax = fig.add_subplot(2 * units_total, 3, 6 * counter + 3)
            draw_occupancy(ax, session.trajectory)
            ax.scatter(unit_pos_0[:, 0], unit_pos_0[:, 1], alpha=0.3, color='#e98a15')
            ax.scatter(unit_pos_1[:, 0], unit_pos_1[:, 1], alpha=0.3, color='#89043d')

            # 4 - FIELD no gain
            omap, smap, fmap, s_firing_map_0 = place_field_2D(pos_at_f0[:, 1:3], unit_fire_0[:, 1:3], sampling_rate,
                                                              0.03)
            ax = fig.add_subplot(2 * units_total, 3, 6 * counter + 4)
            ax.grid(True)
            ax.set_xlabel('X, m')
            ax.set_xlim(limits[0], limits[1])
            ax.set_ylim(limits[2], limits[3])
            ax.imshow(s_firing_map_0.T, cmap='jet', origin='lower', extent=extent)
            ax.axhline(extent_i[2], (extent_i[0] - limits[0]) / lgth_x, (extent_i[1] - limits[0]) / lgth_x, linewidth=2,
                       color='#acbea3')
            ax.axhline(extent_i[3], (extent_i[0] - limits[0]) / lgth_x, (extent_i[1] - limits[0]) / lgth_x, linewidth=2,
                       color='#acbea3')
            ax.axvline(extent_i[0], (extent_i[2] - limits[2]) / lgth_y, (extent_i[3] - limits[2]) / lgth_y, linewidth=2,
                       color='#acbea3')
            ax.axvline(extent_i[1], (extent_i[2] - limits[2]) / lgth_y, (extent_i[3] - limits[2]) / lgth_y, linewidth=2,
                       color='#acbea3')

            # 5 - FIELD with GAIN
            omap, smap, fmap, s_firing_map_0 = place_field_2D(pos_at_f1[:, 1:3], unit_fire_1[:, 1:3], sampling_rate,
                                                              0.03)
            ax = fig.add_subplot(2 * units_total, 3, 6 * counter + 5)
            ax.grid(True)
            ax.set_xlabel('X, m')
            ax.set_xlim(limits[0], limits[1])
            ax.set_ylim(limits[2], limits[3])
            ax.imshow(s_firing_map_0.T, cmap='jet', origin='lower', extent=extent)
            ax.axhline(extent_i[2], (extent_i[0] - limits[0]) / lgth_x, (extent_i[1] - limits[0]) / lgth_x, linewidth=2,
                       color='#acbea3')
            ax.axhline(extent_i[3], (extent_i[0] - limits[0]) / lgth_x, (extent_i[1] - limits[0]) / lgth_x, linewidth=2,
                       color='#acbea3')
            ax.axvline(extent_i[0], (extent_i[2] - limits[2]) / lgth_y, (extent_i[3] - limits[2]) / lgth_y, linewidth=2,
                       color='#acbea3')
            ax.axvline(extent_i[1], (extent_i[2] - limits[2]) / lgth_y, (extent_i[3] - limits[2]) / lgth_y, linewidth=2,
                       color='#acbea3')

            # 6 - FIELD with GAIN - VR coords
            omap, smap, fmap, s_firing_map_1 = place_field_2D(inflate(pos_at_f1[:, 1:3]), unit_pos_1, sampling_rate,
                                                              0.03)

            ax = fig.add_subplot(2 * units_total, 3, 6 * counter + 6)
            ax.grid(True)
            ax.set_xlabel('X, m')
            ax.set_xlim(limits[0], limits[1])
            ax.set_ylim(limits[2], limits[3])
            ax.imshow(s_firing_map_1.T, cmap='jet', origin='lower', extent=extent_i)
            ax.axhline(extent[2], (extent[0] - limits[0]) / lgth_x, (extent[1] - limits[0]) / lgth_x, linewidth=2,
                       color='#acbea3')
            ax.axhline(extent[3], (extent[0] - limits[0]) / lgth_x, (extent[1] - limits[0]) / lgth_x, linewidth=2,
                       color='#acbea3')
            ax.axvline(extent[0], (extent[2] - limits[2]) / lgth_y, (extent[3] - limits[2]) / lgth_y, linewidth=2,
                       color='#acbea3')
            ax.axvline(extent[1], (extent[2] - limits[2]) / lgth_y, (extent[3] - limits[2]) / lgth_y, linewidth=2,
                       color='#acbea3')

            counter += 1

    fig.tight_layout()
    return fig


def gain_3_comparison_plot(session, ttd, unit, an_traj_0, an_traj_1, an_traj_2, an_traj_0_idxs, an_traj_1_idxs,
                           an_traj_2_idxs):
    # ttd_map = {}
    # units_total = len([unit for ttd in ttd_map.values() for unit in ttd])

    units_total = 1  # FIXME
    fig = figure(figsize=(4 * 4, 2 * 6 * (units_total)))

    limits = get_figure_size(session.trajectory[:, 1:3], border=0.1)
    limits = list(limits)
    limits[3] += 0.7

    extent = np.array(get_extent(session.trajectory[:, 1:3]))
    trajectory = session.trajectory

    # defining center of trajectories
    x = (an_traj_1[:, 1].min() + an_traj_1[:, 1].max()) / 2
    y = (an_traj_1[:, 2].min() + an_traj_1[:, 2].max()) / 2
    half_arena = (an_traj_1[:, 2].max() - an_traj_1[:, 2].min()) / 2

    counter = 0
    sampling_rate = 50

    def inflate(traj, gain=1.5):
        res = np.array(traj)

        res[:, 1] -= y
        res[:, 1] = res[:, 1] * gain
        res[:, 1] += y
        res[:, 1] += half_arena / 2

        return res

    def shift(traj):
        res = np.array(traj)

        res[:, 1] += half_arena

        return res

    def draw_occupancy(ax, traj):
        ax.grid(True)
        ax.set_xlabel('X, m')
        ax.set_xlim(limits[0], limits[1])
        ax.set_ylim(limits[2], limits[3])
        ax.scatter(traj[:, 0], traj[:, 1], s=10, alpha=0.05, color='#acbea3')

    def downsample(traj):
        pos_at_f = [traj[0]]
        for pos in traj:
            if pos[0] - pos_at_f[-1][0] > (1 / float(sampling_rate)):
                pos_at_f.append(pos)

        return np.array(pos_at_f)

    # for ttd, units in ttd_map.items():
    #     for unit in units:

    unit_fire_0 = session.get_positions_for_unit2(ttd, unit, an_traj_0_idxs)
    unit_fire_1 = session.get_positions_for_unit2(ttd, unit, an_traj_1_idxs)
    unit_fire_2 = session.get_positions_for_unit2(ttd, unit, an_traj_2_idxs)

    pos_at_f0 = downsample(an_traj_0)
    pos_at_f1 = downsample(an_traj_1)
    pos_at_f2 = downsample(an_traj_2)

    # NO GAIN session remains unchanged
    unit_pos_0 = unit_fire_0[:, 1:3]

    # GAIN session is inflated 1.5 times
    unit_pos_1 = inflate(unit_fire_1[:, 1:3])

    # NO GAIN upper position
    unit_pos_2 = shift(unit_fire_2[:, 1:3])

    extent_i = get_extent(inflate(pos_at_f1[:, 1:3]))
    lgth_x = limits[1] - limits[0]
    lgth_y = limits[3] - limits[2]

    # 1 - firing no gain
    ax = fig.add_subplot(2 * units_total, 3, 6 * counter + 1)
    draw_occupancy(ax, an_traj_0[:, 1:3])
    ax.set_ylabel('%s / %s, Y, m' % (ttd, unit))
    ax.scatter(unit_fire_0[:, 1], unit_fire_0[:, 2], alpha=0.3, color='#e98a15')

    # 2 - firing with gain
    ax = fig.add_subplot(2 * units_total, 3, 6 * counter + 2)
    draw_occupancy(ax, inflate(an_traj_1[:, 1:3]))
    ax.scatter(unit_pos_1[:, 0], unit_pos_1[:, 1], alpha=0.3, color='#89043d')

    # 3 - firing no GAIN upper:
    ax = fig.add_subplot(2 * units_total, 3, 6 * counter + 3)
    draw_occupancy(ax, shift(an_traj_2[:, 1:3]))
    ax.scatter(unit_pos_2[:, 0], unit_pos_2[:, 1], alpha=0.3, color='#003b36')

    # 4 - FIELD no gain
    omap, smap, fmap, s_firing_map_0 = place_field_2D(pos_at_f0[:, 1:3], unit_fire_0[:, 1:3], sampling_rate, 0.03)
    ax = fig.add_subplot(2 * units_total, 3, 6 * counter + 4)
    ax.grid(True)
    ax.set_xlabel('X, m')
    ax.set_xlim(limits[0], limits[1])
    ax.set_ylim(limits[2], limits[3])
    ax.imshow(s_firing_map_0.T, cmap='jet', origin='lower', extent=extent)

    # 5 - FIELD with GAIN
    omap, smap, fmap, s_firing_map_0 = place_field_2D(inflate(pos_at_f1[:, 1:3]), unit_pos_1, sampling_rate, 0.03)
    ax = fig.add_subplot(2 * units_total, 3, 6 * counter + 5)
    ax.grid(True)
    ax.set_xlabel('X, m')
    ax.set_xlim(limits[0], limits[1])
    ax.set_ylim(limits[2], limits[3])
    ax.imshow(s_firing_map_0.T, cmap='jet', origin='lower', extent=extent_i)

    # 6 - FIELD with GAIN - VR coords
    omap, smap, fmap, s_firing_map_1 = place_field_2D(shift(pos_at_f2[:, 1:3]), unit_pos_2, sampling_rate, 0.03)

    ax = fig.add_subplot(2 * units_total, 3, 6 * counter + 6)
    ax.grid(True)
    ax.set_xlabel('X, m')
    ax.set_xlim(limits[0], limits[1])
    ax.set_ylim(limits[2], limits[3])
    ax.imshow(s_firing_map_1.T, cmap='jet', origin='lower', extent=get_extent(shift(pos_at_f2[:, 1:3])))

    counter += 1

    fig.tight_layout()
    return fig