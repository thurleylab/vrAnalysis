# draw on a given axes


def draw_place_field(ax, firing_map, limits=None, extent=None):
    ax.set_xlabel('X, m')
    ax.set_ylabel('Y, m')

    ax.grid(True)

    if limits:
        ax.set_xlim(limits[0], limits[1])
        ax.set_ylim(limits[2], limits[3])

    return ax.imshow(firing_map.T, cmap='jet', origin='lower', extent=extent)


def draw_place_firing(ax, all_pos, unit_pos, limits=None):
    ax.set_xlabel('X, m')
    ax.set_ylabel('Y, m')

    ax.grid(True)

    if limits:
        ax.set_xlim(limits[0], limits[1])
        ax.set_ylim(limits[2], limits[3])

    ax.scatter(all_pos[:, 0], all_pos[:, 1], alpha=0.05, color='#B4B8AB')
    ax.scatter(unit_pos[:, 0], unit_pos[:, 1], color='#9b1d20')


def draw_autocorrelogram(ax, values, color='b'):
    ax.set_xlabel('time, ms')
    ax.set_ylabel('ISI, count')

    ax.grid(True)

    ax.bar(range(len(values)), values, color=color)


def draw_occupancy(ax, all_pos, size=10, color='#acbea3', limits=None):
    ax.set_xlabel('X, m')
    ax.set_ylabel('Y, m')

    ax.grid(True)

    if limits:
        ax.set_xlim(limits[0], limits[1])
        ax.set_ylim(limits[2], limits[3])

    return ax.scatter(all_pos[:, 0], all_pos[:, 1], s=size, alpha=0.05, color=color)
