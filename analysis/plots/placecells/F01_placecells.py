import numpy as np

from .utils import get_figure_size


def unit_place_firing(all_pos, unit_pos_dict, per_row=2, ioff=False):
    """
    Place firing plots for multiple units

    :param all_pos:
    :param unit_pos_dict:  {'1':  # electrode
                                {'2': array([93772, 138840, 165903, ...]),      # unit
                                 '3': array([93772, 138840, 165903, ...]),}     # unit
                            '4':
                                {'2': array([93772, 138840, 165903, ...]),
                                 '3': array([93772, 138840, 165903, ...]),} }
    :return:               figure
    """

    if ioff:
        import matplotlib
        #matplotlib.pyplot.ioff()  # FIXME make this work
        matplotlib.use('Agg')

    from matplotlib.pyplot import figure

    unit_num = len([unit for ttd in unit_pos_dict.values() for unit in ttd.values()])

    if unit_num == 0:
        print('No units found for this session..')
        return

    ax_x_axes = per_row
    ax_y_axes = int(np.ceil(float(unit_num) / ax_x_axes))

    fig = figure(figsize=(int(16 / per_row) * ax_x_axes, int(16 / per_row) * ax_y_axes))
    ctr = 0

    x1, x2, y1, y2 = get_figure_size(all_pos)

    for ttd, units in unit_pos_dict.items():
        if len(units.values()) > 0:
            print('\rProcessing unit group %s..' % str(ttd))

        for unit_id, unit_pos in units.items():
            ctr += 1
            ax = fig.add_subplot(ax_y_axes, ax_x_axes, ctr)

            ax.set_xlabel('X, m')
            ax.set_ylabel('Y, m')

            ax.set_xlim((x1, x2))
            ax.set_ylim((y1, y2))

            ax.grid(True)

            ax.scatter(all_pos[:, 0], all_pos[:, 1], alpha=0.05, color='y')
            ax.scatter(unit_pos[:, 1], unit_pos[:, 2])
            # ax.scatter(session.hotspots[:, 2], session.hotspots[:, 3], s=100, alpha=0.4, color='r')

            ax.set_title("Electrode: %s, Unit: %s" % (ttd, unit_id), fontsize=14)

    return fig


def unit_place_firing_with_autocorr(all_pos, unit_pos_dict, per_row=2, ioff=False):
    """
    Place firing plots for multiple units

    :param all_pos:
    :param unit_pos_dict:  {'1':  # electrode
                                {'2': array([93772, 138840, 165903, ...]),      # unit
                                 '3': array([93772, 138840, 165903, ...]),}     # unit
                            '4':
                                {'2': array([93772, 138840, 165903, ...]),
                                 '3': array([93772, 138840, 165903, ...]),} }
    :return:               figure
    """

    if ioff:
        import matplotlib
        #matplotlib.pyplot.ioff()  # FIXME make this work
        matplotlib.use('Agg')

    from matplotlib.pyplot import figure

    unit_num = len([unit for ttd in unit_pos_dict.values() for unit in ttd.values()])

    if unit_num == 0:
        print('No units found for this session..')
        return

    ax_x_axes = per_row
    ax_y_axes = int(np.ceil(float(unit_num * 2) / ax_x_axes))

    fig = figure(figsize=(int(16 / per_row) * ax_x_axes, int(16 / per_row) * ax_y_axes))
    ctr = 0

    x1, x2, y1, y2 = get_figure_size(all_pos)

    for ttd, units in unit_pos_dict.items():
        if len(units.values()) > 0:
            print('\rProcessing unit group %s..' % str(ttd))  # , end=""

        for unit_id, unit_pos in units.items():
            ctr += 1
            ax = fig.add_subplot(ax_y_axes, ax_x_axes, ctr)

            ax.set_xlabel('X, m')
            ax.set_ylabel('Y, m')

            ax.set_xlim((x1, x2))
            ax.set_ylim((y1, y2))

            ax.grid(True)

            # TODO add colors [#F46036, #0E0E1E, #1B998B]
            ax.scatter(all_pos[:, 0], all_pos[:, 1], alpha=0.05, color='y')
            ax.scatter(unit_pos[0][:, 1], unit_pos[0][:, 2])
            # ax.scatter(session.hotspots[:, 2], session.hotspots[:, 3], s=100, alpha=0.4, color='r')

            ax.set_title("Electrode: %s, Unit: %s" % (ttd, unit_id), fontsize=14)

            ctr += 1
            ax = fig.add_subplot(ax_y_axes, ax_x_axes, ctr)

            ax.set_xlabel('time, ms')
            ax.set_ylabel('ISI, count')

            ax.grid(True)
            ax.bar(range(len(unit_pos[1])), unit_pos[1])

            ax.set_title("Electrode: %s, Unit: %s" % (ttd, unit_id), fontsize=14)

    fig.tight_layout()

    return fig


def unit_place_firing_aBc(all_pos, unit_pos, title, ioff=False):
    """
    :param all_pos:
    :param unit_pos_dict:
    :return:
    """

    if ioff:
        import matplotlib
        matplotlib.use('Agg')

    from matplotlib.pyplot import figure

    ax_x_axes = 2
    ax_y_axes = 1

    fig = figure(figsize=(8 * ax_x_axes, 8 * ax_y_axes))
    x1, x2, y1, y2 = get_figure_size(all_pos)

    ax_A = fig.add_subplot(ax_y_axes, ax_x_axes, 1)
    ax_B = fig.add_subplot(ax_y_axes, ax_x_axes, 2)

    for ax in (ax_A, ax_B):
        ax.set_xlabel('X, m')
        ax.set_ylabel('Y, m')

        ax.set_xlim((x1, x2))
        ax.set_ylim((y1, y2))

        ax.grid(True)

    y_min = np.min(all_pos[:, 1])
    y_max = np.max(all_pos[:, 1])
    y_delta = np.abs(y_min - y_max)

    all_pos_a = all_pos[(all_pos[:, 1] > y_max - y_delta / 3)]
    all_pos_B = all_pos[(all_pos[:, 1] < y_max - y_delta / 3) & (all_pos[:, 1] > y_min + y_delta / 3)]
    all_pos_c = all_pos[(all_pos[:, 1] < y_min + y_delta / 3)]

    unit_pos_a = unit_pos[(unit_pos[:, 1] > y_max - y_delta / 3)]
    unit_pos_B = unit_pos[(unit_pos[:, 1] < y_max - y_delta / 3) & (unit_pos[:, 1] > y_min + y_delta / 3)]
    unit_pos_c = unit_pos[(unit_pos[:, 1] < y_min + y_delta / 3)]

    ax_A.scatter(all_pos_a[:, 0], all_pos_a[:, 1], alpha=0.05, color='y')
    ax_A.scatter(all_pos_B[:, 0], all_pos_B[:, 1], alpha=0.05, color='lightsalmon')
    ax_A.scatter(all_pos_c[:, 0], all_pos_c[:, 1], alpha=0.05, color='y')
    ax_A.scatter(unit_pos[:, 0], unit_pos[:, 1])
    ax_A.set_title(title, fontsize=14)

    all_pos_a_2 = np.array([(x[0], x[1] - y_delta / 6) for x in all_pos_a])
    all_pos_B_2 = np.array([(x[0], y_max - y_delta / 2) for x in all_pos_B])
    all_pos_c_2 = np.array([(x[0], x[1] + y_delta / 6) for x in all_pos_c])

    unit_pos_a_2 = np.array([(x[0], x[1] - y_delta / 6) for x in unit_pos_a])
    unit_pos_B_2 = np.array([(x[0], y_max - y_delta / 2) for x in unit_pos_B])
    unit_pos_c_2 = np.array([(x[0], x[1] + y_delta / 6) for x in unit_pos_c])

    ax_B.scatter(all_pos_a_2[:, 0], all_pos_a_2[:, 1], alpha=0.05, color='y')
    ax_B.scatter(all_pos_B_2[:, 0], all_pos_B_2[:, 1], alpha=0.05, color='y')
    ax_B.scatter(all_pos_c_2[:, 0], all_pos_c_2[:, 1], alpha=0.05, color='y')
    ax_B.scatter(unit_pos_a_2[:, 0], unit_pos_a_2[:, 1])
    ax_B.scatter(unit_pos_B_2[:, 0], unit_pos_B_2[:, 1])
    ax_B.scatter(unit_pos_c_2[:, 0], unit_pos_c_2[:, 1])
    ax_B.set_title(title, fontsize=14)

    return fig


def double_place_field_plot(all_pos_A, unit_pos_A, all_pos_B, unit_pos_B, title, ioff=False):
    """
    :param all_pos:
    :param unit_pos:
    :return:
    """

    if ioff:
        import matplotlib
        matplotlib.use('Agg')

    from matplotlib.pyplot import figure

    ax_x_axes = 2
    ax_y_axes = 1

    fig = figure(figsize=(8 * ax_x_axes, 8 * ax_y_axes))
    x1, x2, y1, y2 = get_figure_size(all_pos_A)

    ax_A = fig.add_subplot(ax_y_axes, ax_x_axes, 1)
    ax_B = fig.add_subplot(ax_y_axes, ax_x_axes, 2)

    for ax in (ax_A, ax_B):
        ax.set_xlabel('X, m')
        ax.set_ylabel('Y, m')

        ax.set_xlim((x1, x2))
        ax.set_ylim((y1, y2))

        ax.grid(True)

    # for i in range(10):  # 10 minutes of recording
    ax_A.scatter(all_pos_A[:, 0], all_pos_A[:, 1], alpha=0.05, color='y')
    ax_A.scatter(unit_pos_A[:, 0], unit_pos_A[:, 1])
    ax_A.set_title(title, fontsize=14)

    ax_B.scatter(all_pos_B[:, 0], all_pos_B[:, 1], alpha=0.05, color='lightsalmon')
    ax_B.scatter(unit_pos_B[:, 0], unit_pos_B[:, 1])
    ax_B.set_title(title, fontsize=14)

    return fig


def units_per_session(session, ioff=False):
    running_idxs = session.get_running_indexes()

    def get_positions_for_unit(ttd, unit):
        positions = []

        for spk_idx in session.tetrodes[ttd][unit]:
            try:
                t_spike = session.get_time_by_spike_index(spk_idx)  # spk_idx in ephys samples
                pos_idx = session.position_index_at(t_spike)

                if np.any(running_idxs == pos_idx):
                    positions.append(session.trajectory[pos_idx])
            except IndexError:
                pass  # outside of recording intervals

        return np.array(positions)

    all_pos = session.trajectory[:, 1:3]
    unit_pos_dict = {}

    for ttd, units in session.tetrode_unit_map.items():
        positions = {}
        for unit in units:
            positions[unit] = get_positions_for_unit(ttd, unit)

        unit_pos_dict[ttd] = positions

    return unit_place_firing(all_pos, unit_pos_dict, ioff=ioff)


def units_per_session_180(session, ttd_idx, unit_idx, time_of_switch, ioff=False):
    running_idxs = session.get_running_indexes()[0]

    t0 = session.trajectory[0][0]
    switch_idx = session.position_index_at(t0 + time_of_switch)

    def get_positions_for_unit(ttd, unit, allowed_idxs):
        positions = []

        for spk_idx in session.tetrodes[ttd][unit]:
            try:
                t_spike = session.get_time_by_spike_index(spk_idx)  # spk_idx in ephys samples
                pos_idx = session.position_index_at(t_spike)

                if pos_idx in allowed_idxs:
                    positions.append(session.trajectory[pos_idx])
            except IndexError:
                pass  # outside of recording intervals

        return np.array(positions)

    all_pos_A = session.trajectory[:switch_idx, 1:3]
    all_pos_B = session.trajectory[switch_idx:, 1:3]

    allowed_idxs_A = running_idxs[np.where(running_idxs < switch_idx)]
    positions_A = get_positions_for_unit(ttd_idx, unit_idx, allowed_idxs_A)[:, 1:3]

    allowed_idxs_B = running_idxs[np.where(running_idxs > switch_idx)]
    positions_B = get_positions_for_unit(ttd_idx, unit_idx, allowed_idxs_B)[:, 1:3]

    title = "Electrode: %s, Unit: %s" % (ttd_idx, unit_idx)

    return unit_place_firing_180(all_pos_A, all_pos_B, positions_A, positions_B, title, ioff=ioff)


def units_per_session_aBc(session, ttd_idx, unit_idx, time_of_switch, ioff=False):
    running_idxs = session.get_running_indexes()[0]

    t0 = session.trajectory[0][0]

    def get_positions_for_unit(ttd, unit, allowed_idxs):
        positions = []

        for spk_idx in session.tetrodes[ttd][unit]:
            try:
                t_spike = session.get_time_by_spike_index(spk_idx)  # spk_idx in ephys samples
                pos_idx = session.position_index_at(t_spike)

                if pos_idx in allowed_idxs:
                    positions.append(session.trajectory[pos_idx])
            except IndexError:
                pass  # outside of recording intervals

        return np.array(positions)

    phi = np.pi * 4.3 / 180.0
    rot_m = np.array([[np.cos(phi), -np.sin(phi)], [np.sin(phi), np.cos(phi)]])

    all_pos = session.trajectory[:, 1:3]
    all_pos_rot = np.array([np.dot(rot_m, x) for x in all_pos])

    unit_pos = get_positions_for_unit(ttd_idx, unit_idx, running_idxs)
    unit_pos_rot = np.array([np.dot(rot_m, x) for x in unit_pos[:, 1:3]])

    title = "Electrode: %s, Unit: %s" % (ttd_idx, unit_idx)

    return unit_place_firing_aBc(all_pos_rot, unit_pos_rot, title, ioff=ioff)