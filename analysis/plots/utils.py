import numpy as np

# colors
# https://coolors.co/336688-d95d39-e3b505-74a57f-e7ecef
def get_colors():
    return {
        'a': '#336688',
        'b': '#D95D39',
        'c': '#74A57F',
        'd': '#E3B505',
        'e': '#E7ECEF',
        'f': '#B4B8AB'
    }


def get_figure_size(arr, border=0.1):
    x_min = arr[:, 0].min()
    x_max = arr[:, 0].max()
    y_min = arr[:, 1].min()
    y_max = arr[:, 1].max()

    x_diff = abs(x_min - x_max)
    y_diff = abs(y_min - y_max)

    x_mid = x_max - x_diff / 2
    y_mid = y_max - y_diff / 2

    # x_dist = (1 + 2 * border) * (x_diff / 2)
    # y_dist = (1 + 2 * border) * (y_diff / 2)

    x_dist = border + x_diff / 2
    y_dist = border + y_diff / 2

    #return x_mid - dist, x_mid + dist, y_mid - dist, y_mid + dist
    return x_mid - x_dist, x_mid + x_dist, y_mid - y_dist, y_mid + y_dist


def get_extent(all_pos):
    return [all_pos[:, 0].min(), all_pos[:, 0].max(), all_pos[:, 1].min(), all_pos[:, 1].max()]
