import numpy as np

from matplotlib.pyplot import figure


def performance(intervals, distances, title=None):
    """
    Plots a single line x, y.
    :param x:   x-values
    :param y:   y-values
    """
    fig = figure(figsize=(15, 10))

    ax1 = fig.add_subplot(2, 1, 1)

    ax1.set_xlabel('reward number')
    ax1.set_ylabel('IRI, s')
    ax1.grid(True)

    ax1.bar(np.arange(len(intervals)) + 1, intervals, alpha=0.4, color='b', label='Rewards')

    ax2 = fig.add_subplot(2, 1, 2)

    ax2.set_xlabel('reward number')
    ax2.set_ylabel('IRD, m')
    ax2.grid(True)

    ax2.bar(np.arange(len(distances)) + 1, distances, alpha=0.4, color='r', label='Rewards')

    if title:
        fig.canvas.set_window_title(title)

    return fig
