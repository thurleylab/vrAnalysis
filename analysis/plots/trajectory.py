from matplotlib.pyplot import figure


def trajectory_scatter(x, y, x_lim=(-2.0, 2.0), y_lim=(-2.0, 2.0), title=None):
    fig = figure(figsize=(10, 10))

    ax = fig.add_subplot(111)

    ax.set_xlabel('X, m')
    ax.set_ylabel('Y, m')

    ax.set_xlim(*x_lim)
    ax.set_ylim(*y_lim)

    ax.plot(x, y, 'o', alpha=0.1, color='y')
    ax.grid(True)

    if title:
        fig.canvas.set_window_title(title)

    return fig


def spikes_vs_place(units, x_lim=(-2.0, 2.0), y_lim=(-2.0, 2.0), title=None):
    fig = figure(figsize=(10, 10))

    ax = fig.add_subplot(111)

    ax.set_xlabel('X, m')
    ax.set_ylabel('Y, m')

    ax.set_xlim(*x_lim)
    ax.set_ylim(*y_lim)
    ax.grid(True)

    for unit in units:
        ax.scatter(unit[:, 1], unit[:, 2])

    if title:
        fig.canvas.set_window_title(title)

    return fig


def arena_vs_animal_positions(ar_traj, an_traj_0, an_traj_1):
    """
    Plots animal (Z) vs arena positions in conditions A vs B

    :param ar_traj:     arena positions (X, Y, Z)
    :param an_traj_0:   animal positions (X, Y, Z) in condition A
    :param an_traj_1:   animal positions (X, Y, Z) in condition B
    :return:            figure
    """
    fig = figure(figsize=(12, 8))

    ax = fig.add_subplot(1, 1, 1)
    ax.set_xlabel('time (s)')
    ax.set_ylabel('position, m')
    ax.set_xlim(0, ar_traj[-1][0] - ar_traj[0][0])
    ax.grid(True)

    # ax.scatter(ar_traj_0[:, 0] - ar_traj[0][0], ar_traj_0[:, 2] - 0.9, color='#e98a15')
    # ax.scatter(ar_traj_1[:, 0] - ar_traj[0][0], ar_traj_1[:, 2] + 0.9, color='#89043d')

    ax.scatter(an_traj_0[:, 0] - ar_traj[0][0], an_traj_0[:, 2], alpha=0.4, s=5, color='#003b36')
    ax.scatter(an_traj_1[:, 0] - ar_traj[0][0], an_traj_1[:, 2], alpha=0.4, s=5, color='#acbea3')

    ax.plot(ar_traj[:, 0] - ar_traj[0][0], ar_traj[:, 2], color='#003b36')
    ax.set_title("Arena vs Animal positions")

    return fig