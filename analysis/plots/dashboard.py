from matplotlib.pyplot import figure


def dashboard(pos_xlim=(-2, 2), pos_ylim=(-2, 2)):
    fig = figure(figsize=(10, 10))

    ax1 = fig.add_subplot(2, 2, 1)
    ax2 = fig.add_subplot(2, 2, 2)
    ax3 = fig.add_subplot(2, 2, 3)
    ax4 = fig.add_subplot(2, 2, 4)

    ax1.set_xlabel('X, m')
    ax1.set_ylabel('Y, m')
    ax1.set_xlim(pos_xlim)
    ax1.set_ylim(pos_ylim)
    ax1.grid(True)

    ax1.plot([], [], '.', lw=2)

    return fig, [ax1, ax2, ax3, ax4]