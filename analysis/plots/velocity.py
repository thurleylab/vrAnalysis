from matplotlib.pyplot import figure


def single_line(x, y, title=None):
    """
    Plots a single line x, y.
    :param x:   x-values
    :param y:   y-values
    """
    fig = figure(figsize=(15, 7))

    ax = fig.add_subplot(111)

    ax.set_xlabel('time (s)')
    ax.set_ylabel('velocity, m/s')
    ax.plot(x, y, alpha=0.4, color='b')
    ax.grid(True)

    if title:
        fig.canvas.set_window_title(title)

    return fig