import os, sys
import json
import h5py
import itertools as itt
import numpy as np

from ..adapters.hdf5 import H5NAMES
from .epochs import get_epochs


class H5SessionStats:

    def __init__(self, sessionpath):
        self.sessionpath = sessionpath
        self.h5file = os.path.join(self.sessionpath, '%s.h5' % H5NAMES.filebase)

    def has_data(self):
        return len([x for x in os.listdir(self.sessionpath) if '.dat' in x]) > 0

    def has_spikesorted_data(self):
        return len([x for x in os.listdir(self.sessionpath) if '.clu' in x]) > 0

    def has_positions(self):
        return len([x for x in os.listdir(self.sessionpath) if 'positions.traj' in x]) > 0

    def has_h5(self):
        return os.path.exists(os.path.join(self.sessionpath, '%s.h5' % H5NAMES.filebase))

    def has_theta_metrics(self):
        return len([x for x in os.listdir(self.sessionpath) if x.startswith('theta') and x.endswith('h5')]) > 0

    def has_brain_states(self):
        return len([x for x in os.listdir(self.sessionpath) if x.find('sts') > 0 and x.endswith('RUN')]) > 0

    def has_unit_metrics(self):
        if not self.has_h5():
            return False

        with h5py.File(self.h5file, 'r') as f:
            unit_names = [name for name in f['units']]

            for unit in unit_names:
                if not 'mean_firing_rate' in f['units'][unit].attrs:  # TODO fix naming
                    return False
        return True

    def has_firing_maps(self):
        if not self.has_h5():
            return False
        try:
            epochs = get_epochs(self.sessionpath)
        except BaseException:
            return False

        with h5py.File(self.h5file, 'r') as f:
            unit_names = [name for name in f['units']]

            for unit in unit_names:
                for epoch_name in epochs:
                    if not epoch_name in f['units'][unit]:
                        return False

                    if not H5NAMES.firing_rate_map in f['units'][unit][epoch_name]:
                        return False
        return True

    def has_theta_mod(self):
        if not self.has_h5():
            return False

        with h5py.File(self.h5file, 'r') as f:
            unit_names = [name for name in f['units']]

            for unit in unit_names:
                if not 'theta_mod' in f['units'][unit]:
                    return False
        return True

    def has_PF_figures(self):
        unit_number = self.get_unit_number()
        PF_path = os.path.join(self.sessionpath, 'analysis', 'placefields')
        if not os.path.exists(PF_path):
            return False
        return len([x for x in os.listdir(PF_path)]) == unit_number

    def has_theta_phase_precession(self):
        if not self.has_h5():
            return False
        try:
            epochs = get_epochs(self.sessionpath)
        except BaseException:
            return False

        with h5py.File(self.h5file, 'r') as f:
            unit_names = [name for name in f['units']]

            for unit in unit_names:
                for epoch_name in epochs:
                    if not epoch_name in f['units'][unit]:
                        return False

                    if not H5NAMES.tpp in f['units'][unit][epoch_name]:
                        return False
        return True

    def get_epoch_names(self):
        try:
            epochs = get_epochs(self.sessionpath)
        except BaseException:
            return 'no epochs'

        epoch_names = [x for x in epochs.keys()]
        epoch_names.sort()
        return '/'.join(epoch_names)

    def get_unit_number(self):
        if not self.has_h5():
            return 0

        with h5py.File(self.h5file, 'r') as f:
            unit_names = [name for name in f['units']]

        return len(unit_names)

    def get_field_number(self):
        if not self.has_h5():
            return 0

        try:
            epochs = get_epochs(self.sessionpath)
        except BaseException:
            return 0

        field_ctr = 0
        with h5py.File(self.h5file, 'r') as f:
            unit_names = [name for name in f['units']]

            for unit in unit_names:
                for epoch_name in epochs:
                    if not epoch_name in f['units'][unit]:
                        continue

                    if H5NAMES.field_patches in f['units'][unit][epoch_name]:
                        fields = f['units'][unit][epoch_name][H5NAMES.field_patches]
                        field_ctr += len(np.unique(fields)) - 1
        return field_ctr

    def get_pair_number(self):
        if not self.has_h5():
            return 0

        try:
            epochs = get_epochs(self.sessionpath)
        except BaseException:
            return 0

        pair_ctr = 0
        with h5py.File(self.h5file, 'r') as f:
            unit_names = [name for name in f['units']]

            for unit, e1, e2 in itt.product(unit_names, epochs, epochs):
                shift_matrix_name = '%s_%s_%s' % (H5NAMES.field_shifts, e1, e2)
                if shift_matrix_name in f['units'][unit]:
                    shift_matrix = np.array(f['units'][unit][shift_matrix_name])
                    selected = shift_matrix[shift_matrix[:, 2] > 0]
                    if len(selected) > 0:
                        pair_ctr += len(np.unique(selected[:, 0]))

        return pair_ctr
