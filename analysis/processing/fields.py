import numpy as np
import h5py
import os
import datetime
import matlab.engine

from sklearn.cluster import DBSCAN

from .utils import derive_session_type
from .epochs import get_epochs
from ..models.analytics import place_field_2D
from ..models.vr import RatCAVESession


def is_in_periods(t, periods):
    for period in periods:
        if t > period[0] and t < period[1]:
            return True
    return False


def get_at_freq(trajectory, pos_times):
    idxs = []
    traj_pos_cnt = 0
    for i, curr_pos_t in enumerate(pos_times):
        delta_curr = np.abs(curr_pos_t - trajectory[traj_pos_cnt][0])
        delta_next = np.abs(curr_pos_t - trajectory[traj_pos_cnt + 1][0])

        while delta_curr > delta_next:
            traj_pos_cnt += 1
            if traj_pos_cnt + 1 >= len(trajectory):
                break

            delta_curr = np.abs(curr_pos_t - trajectory[traj_pos_cnt][0])
            delta_next = np.abs(curr_pos_t - trajectory[traj_pos_cnt + 1][0])
        else:
            idxs.append(traj_pos_cnt)
            traj_pos_cnt -= 1

    return np.array(idxs)


def get_maps(trajectory, epochs, spike_idxs, running_idxs):
    t_start = trajectory[0][0]
    duration = trajectory[-1][0] - trajectory[0][0]  # in secs
    target_frequency = 50  # Hz
    sample_count = int(duration / (1. / 50.))

    # important to set xy_range the same for all epochs
    xy_range = [
        trajectory[running_idxs][:, 1].min(),
        trajectory[running_idxs][:, 1].max(),
        trajectory[running_idxs][:, 2].min(),
        trajectory[running_idxs][:, 2].max(),
    ]

    pos_times = np.linspace(0, sample_count / 50, sample_count + 1) + t_start
    pos_idxs = get_at_freq(trajectory, pos_times)

    maps = {}
    for name, periods in epochs.items():
        an_traj_at_f = []
        for i, tyzx in enumerate(trajectory[np.intersect1d(pos_idxs, running_idxs)]):
            if is_in_periods(tyzx[0] - t_start, periods):
                an_traj_at_f.append(tyzx)

        spiking = []
        for i, tyzx in enumerate(trajectory[np.intersect1d(spike_idxs, running_idxs)]):
            if is_in_periods(tyzx[0] - t_start, periods):
                spiking.append(tyzx)

        an_traj_at_f = np.array(an_traj_at_f)
        spiking = np.array(spiking)

        if len(spiking) < 1:  # a single spike to build a map
            spiking = np.array([[t_start, 0, 0]])

        now = datetime.datetime.now()
        omap, smap, fmap, s_firing_map = place_field_2D(an_traj_at_f[:, 1:3], spiking[:, 1:3], target_frequency,
                                                        bin_size=0.03, xy_range=xy_range)
        #print('PF 2D %s' % (datetime.datetime.now() - now))

        maps[name] = [omap, smap, fmap, s_firing_map]

    return maps, xy_range


# def patch_positions_outside_arena(session_path, filebase='all'):  # only X axis
#
#     def get_minmax(data):
#         vals, periods = np.histogram(data, 100)  # position distribution
#         pers = periods[:-1][vals > 500]  # thresholding by 500
#         return pers.min(), pers.max()
#
#     h5file = os.path.join(session_path, '%s.h5' % filebase)
#
#     with h5py.File(h5file, 'r+') as f:
#         trajectory = np.array(f['animal_trajectory'])
#         running_idxs = np.array(f['running_idxs'])[0]
#
#         delta = 0.05  # 10 cm larger than arena
#         x_min, x_max = get_minmax(trajectory[:, 1])
#         y_min, y_max = get_minmax(trajectory[:, 2])
#
#         idxs_inside_x = np.where((trajectory[:, 1] < x_max + delta) & (trajectory[:, 1] > x_min - delta))[0]
#         idxs_inside_y = np.where((trajectory[:, 2] < y_max + delta) & (trajectory[:, 1] > y_min - delta))[0]
#         idxs_inside = np.intersect1d(idxs_inside_x, idxs_inside_y)
#         idxs_outside = np.setdiff1d(np.arange(len(trajectory)), idxs_inside)
#
#         delta_x = x_max - x_min
#         delta_y = y_max - y_min
#         if not 0.6 < delta_x < 0.8 or not 1.4 < delta_y < 1.8:
#             raise ValueError("Positions outside arena could not be detected.")
#
#         if len(idxs_outside) > 0:
#             trajectory[idxs_outside, 1] = x_min + (x_max - x_min) / 2  # set X to center
#             trajectory[idxs_outside, 2] = y_min + (y_max - y_min) / 2  # set Y to center
#             new_idxs = np.intersect1d(idxs_inside, running_idxs)
#
#             del f['running_idxs']
#             f.create_dataset('running_idxs', data=np.array([new_idxs]))
#             del f['animal_trajectory']
#             f.create_dataset('animal_trajectory', data=np.array(trajectory))
#
#             print("%s: removed %s positions outside the arena" % (session_path[-20:], len(trajectory) - len(idxs_inside)))
#
#         else:
#             print("%s: no positions outside arena found." % session_path[-20:])


def build_place_fields(session_path, filebase='all'):
    session_type = derive_session_type(session_path)

    print("%s: Detected session type %s" % (session_path[-20:], session_type))

    epochs = get_epochs(session_path)
    h5file = os.path.join(session_path, '%s.h5' % filebase)

    with h5py.File(h5file, 'r') as f:
        unit_names = [name for name in f['units']]
        trajectory = np.array(f['animal_trajectory'])
        traj_times_norm = trajectory[:, 0] - trajectory[0][0]
        running_idxs = np.array(f['running_idxs'])

    for unit_group_name in unit_names:

        electrode = unit_group_name.split('_')[0]
        unit = unit_group_name.split('_')[1]

        # ---- reading from file -----

        with h5py.File(h5file, 'r') as f:
            # get spike indexes
            spike_idxs = np.array(f['units']['%s_%s' % (electrode, unit)]['spike_idxs'])

        # ---- preparing data -----

        traj_idxs = {}
        for name, periods in epochs.items():
            curr_idxs = []
            for i, t in enumerate(traj_times_norm):
                if is_in_periods(t, periods):
                    curr_idxs.append(i)
            traj_idxs[name] = np.intersect1d(np.array(curr_idxs), running_idxs)

        maps, xy_range = get_maps(trajectory, epochs, spike_idxs, running_idxs)

        # ---- storing to the file -----

        with h5py.File(h5file, 'r+') as f:
            unit_group = f['units']['%s_%s' % (electrode, unit)]
            unit_group.attrs['xy_range'] = np.array(xy_range)

            for name, periods in epochs.items():
                # store epoch indexes
                epoch_idxs_name = '%s_idxs' % name
                if epoch_idxs_name in f.keys():
                    del f[epoch_idxs_name]

                f.create_dataset(epoch_idxs_name, data=np.array(traj_idxs[name]))
                f[epoch_idxs_name].attrs['description'] = "Trajectory indexes for condition %s" % name

                # store place field data
                unit_group = f['units']['%s_%s' % (electrode, unit)]

                if not name in unit_group.keys():
                    epoch_grp = unit_group.create_group(name)
                else:
                    epoch_grp = unit_group[name]

                for g_name in ['01_occupancy', '02_spiking', '03_firing_bins', '04_firing_rate_map']:
                    if g_name in epoch_grp.keys():
                        del epoch_grp[g_name]

                epoch_grp.create_dataset('01_occupancy', data=np.array(maps[name][0]))
                epoch_grp.create_dataset('02_spiking', data=np.array(maps[name][1]))
                epoch_grp.create_dataset('03_firing_bins', data=np.array(maps[name][2]))
                epoch_grp.create_dataset('04_firing_rate_map', data=np.array(maps[name][3]))


def do_bootstrapping(session_path, config):

    def get_bootstrapped_for_period(spike_idxs, period_idxs, chunks_number=50):
        """
        We do bootstrapping for each epoch/condition individually, as logically
        spiking in different conditions should not be intermixed.

        spike_idxs - indexes of individual spikes (spiketrain)
        period_idxs - indexes of running in a particular epoch / condition
        """
        chunks = np.linspace(0, len(period_idxs), chunks_number + 1)
        period_idxs.sort()

        result_idxs = []
        for i in range(chunks_number):
            chunk = np.random.randint(0, chunks_number)
            selected_period_idxs = period_idxs[int(chunks[chunk]):int(chunks[chunk + 1])]

            res = np.intersect1d(selected_period_idxs, spike_idxs)
            result_idxs += list(res)

        return np.array(result_idxs)

    # ------------ prepare data -----------

    h5file = os.path.join(session_path, '%s.h5' % config['filebase'])
    with h5py.File(h5file, 'r') as f:
        unit_names = [name for name in f['units']]
        trajectory = np.array(f['animal_trajectory'])
        traj_times_norm = trajectory[:, 0] - trajectory[0][0]
        running_idxs = np.array(f['running_idxs'])

    t_start = trajectory[0][0]
    duration = trajectory[-1][0] - trajectory[0][0]  # in secs
    target_frequency = 50  # Hz
    sample_count = int(duration / (1. / 50.))

    epochs = get_epochs(session_path)

    traj_idxs = {}  # like epochs (name, periods) but with periods as array of indexes
    for name, periods in epochs.items():
        curr_idxs = []
        for i, t in enumerate(traj_times_norm):
            if is_in_periods(t, periods):
                curr_idxs.append(i)
        traj_idxs[name] = np.intersect1d(np.array(curr_idxs), running_idxs)

    # important to set xy_range the same for all epochs
    xy_range = [
        trajectory[running_idxs][:, 1].min(),
        trajectory[running_idxs][:, 1].max(),
        trajectory[running_idxs][:, 2].min(),
        trajectory[running_idxs][:, 2].max(),
    ]

    pos_times = np.linspace(0, sample_count / 50, sample_count + 1) + t_start
    pos_idxs = get_at_freq(trajectory, pos_times)

    h5bootfile = os.path.join(session_path, 'bootstrap.h5')

    for unit_group_name in unit_names:
        now = datetime.datetime.now()
        electrode = unit_group_name.split('_')[0]
        unit = unit_group_name.split('_')[1]

        # skip electrodes if required
        if len(config['bootstrap_electrodes']) > 0 and int(electrode) not in config['bootstrap_electrodes']:
            continue
        # skip units if required
        if len(config['bootstrap_units']) > 0 and int(unit) not in config['bootstrap_units']:
            continue

        # ------ do bootstrapping --------

        with h5py.File(h5bootfile, 'a') as f:
            if not unit_group_name in f:
                f.create_group(unit_group_name)
            f[unit_group_name].attrs['xy_range'] = np.array(xy_range)

            for name in traj_idxs:
                if not name in f[unit_group_name]:
                    f[unit_group_name].create_group(name)

        for boot_count in range(config['bootstrap_count']):  # about 1000 times
            for name, period_idxs in traj_idxs.items():  # get bootstraped fields for each condition

                # reading original spiketrain
                with h5py.File(h5file, 'r') as f:
                    spike_idxs = np.array(f['units']['%s_%s' % (electrode, unit)]['spike_idxs'])

                # clean or keep the existing bootstrapped dataset
                with h5py.File(h5bootfile, 'r+') as f:
                    if 'boot_%s' % (boot_count + 1) in f[unit_group_name][name]:
                        if config['overwrite']:
                            del f[unit_group_name][name]['boot_%s' % (boot_count + 1)]
                        else:
                            continue

                # computing bootstrapped map for a condition
                spike_idxs_bootstrapped = get_bootstrapped_for_period(spike_idxs, period_idxs)

                an_traj_at_f = trajectory[np.intersect1d(pos_idxs, period_idxs)]
                if len(spike_idxs_bootstrapped) < 1:
                    spiking = np.array([[t_start, 0, 0]])  # a single spike to build a map
                else:
                    spiking = trajectory[spike_idxs_bootstrapped]

                omap, smap, fmap, s_firing_map = place_field_2D(an_traj_at_f[:, 1:3], spiking[:, 1:3],
                                                                target_frequency,
                                                                bin_size=0.03, xy_range=xy_range)
                with h5py.File(h5bootfile, 'r+') as f:
                    f[unit_group_name][name].create_dataset('boot_%s' % (boot_count + 1), data=s_firing_map)

        print('computed bootstrapped maps for %s %s, took %s' % (electrode, unit, datetime.datetime.now() - now))


def compute_bootstraped_COMs(session_path, config):

    epochs = get_epochs(session_path)

    h5file = os.path.join(session_path, '%s.h5' % config['filebase'])
    h5bootfile = os.path.join(session_path, 'bootstrap.h5')
    with h5py.File(h5bootfile, 'r') as f:
        unit_names = [name for name in f]

    eng = matlab.engine.start_matlab()
    eng.addpath(config['matlab'])
    eng.addpath(eng.genpath(config['labbox']))

    print('Matlab engine started')

    for unit_group_name in unit_names:
        now = datetime.datetime.now()
        electrode = unit_group_name.split('_')[0]
        unit = unit_group_name.split('_')[1]

        # skip electrodes if required
        if len(config['bootstrap_electrodes']) > 0 and int(electrode) not in config['bootstrap_electrodes']:
            continue
        # skip units if required
        if len(config['bootstrap_units']) > 0 and int(unit) not in config['bootstrap_units']:
            continue

        for condition, periods in epochs.items():
            COMs_matrix = eng.computeCOMs(h5bootfile, electrode, unit, condition)

            if not len(COMs_matrix) > 0:
                continue

            with h5py.File(h5file, 'r+') as f:  # store to the main HDF5 file
                unit_name = '%s_%s' % (electrode, unit)
                if not unit_name in f['units'] or not condition in f['units'][unit_name]:
                    continue  # unit was deleted but is still present in bootstrap - ignore
                cond_group = f['units']['%s_%s' % (electrode, unit)][condition]

                if '05_field_centers_bootstrapped' in cond_group:
                    del cond_group['05_field_centers_bootstrapped']
                cond_group.create_dataset('05_field_centers_bootstrapped', data=COMs_matrix)

        #print('computed bootstrapped field centers for %s %s, took %s' % (electrode, unit, datetime.datetime.now() - now))

    eng.quit()


def do_clustering_and_fields(session_path, config):

    epochs = get_epochs(session_path)
    h5file = os.path.join(session_path, '%s.h5' % config['filebase'])
    with h5py.File(h5file, 'r') as f:
        unit_names = [name for name in f['units']]

    for unit_group_name in unit_names:
        now = datetime.datetime.now()
        electrode = unit_group_name.split('_')[0]
        unit = unit_group_name.split('_')[1]

        with h5py.File(h5file, 'r') as f:
            xy_range = np.array(f['units']['%s_%s' % (electrode, unit)].attrs['xy_range'])

        for j, condition in enumerate(epochs.keys()):

            with h5py.File(h5file, 'r') as f:
                cond_group = f['units']['%s_%s' % (electrode, unit)][condition]
                if not '05_field_centers_bootstrapped' in cond_group:
                    continue
                COMs = np.array(cond_group['05_field_centers_bootstrapped'])
                shape = np.array(cond_group['04_firing_rate_map']).shape

            x_space = np.linspace(xy_range[0], xy_range[1], shape[0] + 1)
            y_space = np.linspace(xy_range[2], xy_range[3], shape[1] + 1)
            f_map = np.zeros(shape)  # future field patches map
            stats = []  # future COMs stats for each cluster

            # ------------ clustering ----------------

            db = DBSCAN(eps=0.03, min_samples=10).fit(COMs[:, 2:4])
            labels = db.labels_

            n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
            n_noise_ = list(labels).count(-1)

            for i in range(n_clusters_):  # i is the cluster number!
                clu = COMs[labels == i]
                if len(clu) < 100:  # skip clusters with less than 100 bootstrap resamples
                    continue

                # ------------ center of masses and stats ------------

                com_x = np.array([x[2] * x[4] for x in clu]).sum() / clu[:, 4].sum()
                com_y = np.array([x[3] * x[4] for x in clu]).sum() / clu[:, 4].sum()

                stats.append([
                    i + 1,  # field (patch) ID
                    com_x,  # X center of mass of this cluster
                    com_y,  # Y center of mass of this cluster
                    round(clu[:, 4].mean(), 3),  # mean PFR of this cluster
                    round(np.std(clu[:, 2]), 4),  # SD for bootstraped COMs for X position
                    round(np.std(clu[:, 3]), 4),  # SD for bootstraped COMs for Y position
                    round(np.std(clu[:, 4]), 4),  # SD for bootstraped PFR
                    len(clu),  # number of points in this cluster
                    n_noise_,  # length of noise
                    len(labels)  # total points
                ])

                # ------------ patches ----------------

                points = []
                for record in clu:
                    x_idx = np.abs(x_space - record[2]).argmin()
                    x_bin_no = x_idx if record[2] > x_space[x_idx] else x_idx - 1
                    y_idx = np.abs(y_space - record[3]).argmin()
                    y_bin_no = y_idx if record[3] > y_space[y_idx] else y_idx - 1

                    if not (x_bin_no, y_bin_no) in points:
                        points.append((x_bin_no, y_bin_no))

                points = np.array(points)
                for idx in points:
                    try:
                        f_map[idx[0]][idx[1]] = i + 1  # field (patch) ID
                    except IndexError:
                        import ipdb
                        ipdb.set_trace()
                        raise ValueError

            # ----------- save to H5 -----------

            with h5py.File(h5file, 'r+') as f:
                cond_group = f['units']['%s_%s' % (electrode, unit)][condition]
                if '06_field_clusters' in cond_group:
                    del cond_group['06_field_clusters']
                if '07_field_patches' in cond_group:
                    del cond_group['07_field_patches']

                cond_group.create_dataset('07_field_patches', data=f_map)
                ds = cond_group.create_dataset('06_field_clusters', data=np.array(stats))
                attr_names = ['field_ID', 'x_COM', 'y_COM', 'PFR_mean', 'x_COM_SD', 'y_COM_SD', 'PFR_SD',
                              'p_cluster', 'p_noise' 'p_total']
                for pos, name in enumerate(attr_names):
                    ds.attrs[name] = pos

        #print('clustering done for %s %s, took %s' % (electrode, unit, datetime.datetime.now() - now))


def compute_shift_matrix(session_path, config):

    epochs = get_epochs(session_path)
    h5file = os.path.join(session_path, '%s.h5' % config['filebase'])
    with h5py.File(h5file, 'r') as f:
        unit_names = [name for name in f['units']]

    for unit_group_name in unit_names:
        now = datetime.datetime.now()
        electrode = unit_group_name.split('_')[0]
        unit = unit_group_name.split('_')[1]

        for j, (epoch_A, epoch_B) in enumerate(config["shift_conditions"]):

            # ------------- reading unit fields data ---------------

            with h5py.File(h5file, 'r') as f:
                unit_group = f['units']['%s_%s' % (electrode, unit)]
                if not epoch_A in unit_group or not epoch_B in unit_group:
                    continue  # requested epochs were not recorded in this session

                fieldsA = np.array(unit_group[epoch_A][config['fields_sffx']] if config['fields_sffx'] in unit_group[epoch_A] else [])
                fieldsB = np.array(unit_group[epoch_B][config['fields_sffx']] if config['fields_sffx'] in unit_group[epoch_B] else [])

                COMa = np.array(unit_group[epoch_A][config['COMs_sffx']]) if config['COMs_sffx'] in unit_group[epoch_A] else None
                COMb = np.array(unit_group[epoch_B][config['COMs_sffx']]) if config['COMs_sffx'] in unit_group[epoch_B] else None

                xy_range = np.array(unit_group.attrs['xy_range'])

            num_fields_A = len(np.unique(fieldsA)) - 1  # 0-zeros are not fields
            num_fields_B = len(np.unique(fieldsB)) - 1  # 0-zeros are not fields

            if (num_fields_A < 1 or num_fields_B < 1) or (COMa is None or COMb is None) or (len(COMa) < 1 or len(COMb) < 1):
                """
                TODO process cases when PF fully
                - disappears (N fields in A and 0 in B), or
                - appears    (0 fields in A and M in B)
                separately
                """
                print('No fields for unit %s' % unit_group_name)
                continue

            # get only 2 fields with max points (see DBSCAN) from each condition.
            sort_idxs = np.argsort(COMa[:, 7])[::-1]  # sort by number of points in the cluster
            field_ids_A = COMa[sort_idxs][:2][:, 0]  # first two field IDs with highest points
            sort_idxs = np.argsort(COMb[:, 7])[::-1]  # sort by number of points in the cluster
            field_ids_B = COMb[sort_idxs][:2][:, 0]  # first two field IDs with highest points

            # shift_in_bins = round(config['shift_in_meters'] / ((xy_range[3] - xy_range[2]) / fieldsA.shape[1]))
            # metrics = []
            # for field_A_id in field_ids_A:
            #     field_data = []
            #
            #     for field_B_id in field_ids_B:
            #         # compute overlap y +0
            #         fA_idxs = np.where(np.flipud(fieldsA.T).flatten() == field_A_id)[0]
            #         fB_idxs = np.where(np.flipud(fieldsB.T).flatten() == field_B_id)[0]
            #         intersection_0 = np.intersect1d(fA_idxs, fB_idxs)
            #
            #         COM1 = COMa[(COMa[:, 0] == field_A_id)][0]
            #         COM2 = COMb[(COMb[:, 0] == field_B_id)][0]
            #         COM_shift_0 = np.sqrt((COM2[1] - COM1[1]) ** 2 + (COM2[2] - COM1[2]) ** 2)
            #
            #         # compute overlap y + shift_in_meters
            #         intersection_30 = np.intersect1d(fA_idxs, fB_idxs + int(shift_in_bins * fieldsB.shape[0]))
            #         COM_shift_30 = np.sqrt((COM2[1] - COM1[1]) ** 2 + (COM2[2] - config['shift_in_meters'] - COM1[2]) ** 2)
            #
            #         y_shift = COM2[2] - COM1[2]  # just vertical shift
            #
            #         # field_A size, field_B size, overlap at 0, overlap at -30, COM shift at 0, COM shift at 30
            #         data = [len(fA_idxs), len(fB_idxs), len(intersection_0), len(intersection_30), COM_shift_0,
            #                 COM_shift_30, y_shift, field_A_id, field_B_id]
            #         field_data.append(data)
            #
            #     metrics.append(field_data)

            shift_Y_in_bins = int(round(config['shift_in_meters'] / ((xy_range[3] - xy_range[2]) / fieldsA.shape[1])))
            shift_matrix = []  # field_A_id, field_B_id, MAX intersection

            for field_A_id in field_ids_A:
                fA_idxs = np.where(np.flipud(fieldsA.T).flatten() == field_A_id)[0]

                for field_B_id in field_ids_B:
                    fB_idxs = np.where(np.flipud(fieldsB.T).flatten() == field_B_id)[0]
                    max_intersection = 0

                    # compute maximum intersection shifting field B direction to shift_Y_in_bins
                    for curr_shift_in_bins in range(2 * abs(shift_Y_in_bins)):  # from -shift_in_meters to shift_in_meters
                        shift = curr_shift_in_bins - abs(shift_Y_in_bins)
                        intersection_flat = np.intersect1d(fA_idxs, fB_idxs + int(shift * fieldsB.shape[0]))
                        intersection_norm = float(len(intersection_flat)) / (float(len(fA_idxs) + len(fB_idxs)) / 2)

                        if intersection_norm > max_intersection:
                            max_intersection = intersection_norm

                    shift_matrix.append([field_A_id, field_B_id, max_intersection])

            with h5py.File(h5file, 'r+') as f:
                unit_group = f['units']['%s_%s' % (electrode, unit)]

                ds_name = "%s_%s_%s" % (config['shift_sffx'], epoch_A, epoch_B)
                if ds_name in unit_group:
                    del unit_group[ds_name]
                unit_group.create_dataset(ds_name, data=np.array(shift_matrix))

        #print('shift matrix done for %s %s, took %s' % (electrode, unit, datetime.datetime.now() - now))


#
# def compute_bootstrapped_mean_CI(session_path, config):
#     now = datetime.datetime.now()
#
#     h5file = os.path.join(session_path, 'all.h5')
#     with h5py.File(h5file, 'r') as f:
#         unit_names = [name for name in f['units']]
#
#     epochs = get_epochs(session_path)
#
#     for unit_group_name in unit_names:
#         electrode = unit_group_name.split('_')[0]
#         unit = unit_group_name.split('_')[1]
#
#         # ---- reading from file -----
#
#         for name in epochs.keys():
#             with h5py.File(h5file, 'r+') as f:
#                 if not name + '_raw_boot_COMs' in f['units']['%s_%s' % (electrode, unit)]:
#                     continue
#                 boot_raw = np.array(f['units']['%s_%s' % (electrode, unit)][name + '_raw_boot_COMs'])
#
#             # mean, CI and SD: field ID, X, Y, pear rate R, CI < 5 for X, CI > 5 for X, etc.
#             mean_matrix = np.zeros((len(np.unique(boot_raw[:, 1])), 13))
#             for i in np.unique(boot_raw[:, 1]):
#                 field_idx = int(i) - 1
#                 selected = boot_raw[boot_raw[:, 1] == i]
#                 mean_matrix[field_idx:, 0] = i
#                 mean_matrix[field_idx:, 1] = selected[:, 2].mean()
#                 mean_matrix[field_idx:, 2] = selected[:, 3].mean()
#                 mean_matrix[field_idx:, 3] = selected[:, 4].mean()
#                 mean_matrix[field_idx:, 4] = np.percentile(selected[:, 2], 5)
#                 mean_matrix[field_idx:, 5] = np.percentile(selected[:, 2], 95)
#                 mean_matrix[field_idx:, 6] = np.percentile(selected[:, 3], 5)
#                 mean_matrix[field_idx:, 7] = np.percentile(selected[:, 3], 95)
#                 mean_matrix[field_idx:, 8] = np.percentile(selected[:, 4], 5)
#                 mean_matrix[field_idx:, 9] = np.percentile(selected[:, 4], 95)
#                 mean_matrix[field_idx:, 10] = selected[:, 2].std()
#                 mean_matrix[field_idx:, 11] = selected[:, 3].std()
#                 mean_matrix[field_idx:, 12] = selected[:, 4].std()
#
#             with h5py.File(h5file, 'r+') as f:
#                 unit_group = f['units']['%s_%s' % (electrode, unit)]
#
#                 # save mean and CIs
#                 if name + '_fields_boot_COMs' in unit_group.keys():
#                     del unit_group[name + '_fields_boot_COMs']
#                 unit_group.create_dataset(name + '_fields_boot_COMs', data=mean_matrix)
#
#     print('computed bootstrapped Mean/CIs for %s, took %s' % (session_path[-20:], datetime.datetime.now() - now))


def clean_h5_elements(session_path):
    h5file = os.path.join(session_path, 'all.h5')

    f = h5py.File(h5file, 'r+')
    epochs = get_epochs(session_path)

    for unit_group_name in f['units']:
        pass

    # for name in epochs.keys():
    #     try:
    #         del f['units'][unit_group_name][nA+'_fields']
    #     except:
    #         pass

    f.close()
