h5path = 'storage2/andrey/data/processed/003909/2020-03-28_15-45-33/all.h5';
placeField = h5read(h5path, ['/units/' '4_4' '/' 'A_place_field']);
placeField = h5read(h5path, ['/units/' '4_4' '/' 'C_place_field']);


figure;
mph = 0.2 * max(placeField(:));  % placeField is a 2D firing rates matrix
prominences = [0.5, 1.0, 1.5];

for i=1:3
    [pks, locs] = findpeaks(placeField(:), 'MinPeakHeight', mph, 'MinPeakProminence', prominences(i));
    [r,c] = ind2sub(size(placeField), locs);

    ax = subplot(1,3,i);
    imagesc(placeField, 'Parent', ax);
    hold on;
    plot(c, r, 'r+', 'Parent', ax);
end
