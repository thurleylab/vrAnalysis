import os
import json
import numpy as np
import pandas as pd


def get_params_from_json(session_path):
    try:
        jsonfile = [x for x in os.listdir(session_path) if '.json' in x][0]

        with open(os.path.join(session_path, jsonfile)) as f:
            config = json.load(f)

        return config
    except IndexError:
        return {}


def derive_session_type(path):
    naming = {
        'ABBA': 'ABBA',
        'sphere': 'sphere',
        'VFG': 'GAIN',
        'VShiftA': 'SHIFT',
        'Y': 'Y',
        'islands': 'islands'
    }

    try:
        maze_type = [x for x in os.listdir(path) if '.obj' in x][0]
        s_type = maze_type.split('.')[0]

        conf = get_params_from_json(path)['vr']['scene']
        name = naming[s_type]

        if s_type == 'VFG':
            if 'transition_duration' in conf:
                name += ' 3 parts'
                if 'gain' in conf:
                    if conf['gain'] < 0.3:
                        name += ' 1.2x'
                    else:
                        name += ' 1.4x'
            else:
                name += ' centered'

                # AWAY or 30
                if ('gain_switch_freq' in conf and conf['gain_switch_freq'] > 0.03) \
                        or ('gain_switch_interval' in conf and conf['gain_switch_interval'] < 120):
                    name += ' 30'
                else:
                    name += ' AWAY'

        elif s_type == 'VShiftA':
            if 'move_type' in conf and conf['move_type'] == 'visual':
                name += ' visual'
                if 'z_move_visual' in conf and conf['z_move_visual'] < -0.4:
                    name += ' long'
            else:
                if 'is_locked' in conf and conf['is_locked']:
                    name += ' both'
                else:
                    #name += ' physical'

                    # AWAY or 30
                    if ('move_freq' in conf and conf['move_freq'] > 0.03) \
                            or ('move_interval' in conf and conf['move_interval'] < 70):
                        name += ' periodic'
                    else:
                        name += ' single'

            if 'timers' in conf and 'turn_projection_off' in conf['timers'] and \
                            conf['timers']['stop_experiment'] > conf['timers']['turn_projection_off']:
                name += ' +dark'

        elif s_type == 'Y':
            # 3 or alternate
            if ('alternate' in conf and conf['alternate']):
                name += ' Alt'
            else:
                name += ' XYZ'

        return name

    except IndexError:
        return 'sleep'


def to_short(session_type):
    conversion_table = {
        'SHIFT physical 30': 'SP',
        'SHIFT physical 30 +dark': 'SPd',
        'SHIFT both': 'SB',
        'SHIFT both +dark': 'SBd',
        'SHIFT visual': 'SV',
        'GAIN 3 parts': 'G3'
    }

    if session_type in conversion_table:
        return conversion_table[session_type]
    return session_type


def get_sessions_list(where, animals, s_type=None, short_type=False):
    """
    Returns animal/folder list of session of a given type
    """
    unit_dataset = []

    for animal in animals:
        sessionspath = os.path.join(where, animal)
        session_dirs = [x for x in os.listdir(sessionspath) if os.path.isdir(os.path.join(sessionspath, x))]

        for i, session_dir in enumerate(session_dirs):
            s_path = os.path.join(sessionspath, session_dir)

            unit_dataset.append([
                animal,
                session_dir,
                to_short(derive_session_type(s_path)) if short_type else derive_session_type(s_path)
            ])

    if s_type is None:
        return unit_dataset
    else:
        return [x for x in unit_dataset if x[2] == s_type]


def get_sessions_list_from_file(filepath, s_type):
    """
    Returns animal/folder list of session of a given type
    Reads data from the stats CSV file

    filepath    like '/home/andrey/storage2/andrey/data/projects/unit_stats.csv'
    s_type      like 'SHIFT visual'
    """
    df = pd.read_csv(filepath, dtype=str)

    return set([(row['animal'], row['folder']) for index, row in df[df['type'] == s_type].iterrows()])


def create_img_symlink(dest_folder, source_folder, animal, session, electrode, unit, prefix=""):
    filename = "%s%s_%s_%s_%s.png" % (prefix, animal, session, electrode, unit)
    src_img_path = os.path.join(source_folder, animal, session, 'analysis', 'placefields', filename)
    dst_img_path = os.path.join(dest_folder, filename)

    if not os.path.exists(dst_img_path):
        os.symlink(src_img_path, dst_img_path)


def create_symlinks(dest_folder, source_folder, units_list, prefix=""):
    """
    every item is an (animal, session, electrode, unit)
    """
    for item in units_list:
        create_img_symlink(dest_folder, source_folder, item[0], item[1], item[2], item[3], prefix)
