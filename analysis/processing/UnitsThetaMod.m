function out = UnitsThetaMod(SessionPath, FileBase)
% unit theta modulation 
out = 0;

% dependencies on toolboxes
addpath(genpath('/storage/share/matlab/labbox'));
%addpath(genpath('/storage/share/matlab/Third-Party_Toolboxes/FMAToolbox'))

State = 'RUN';
h5path = fullfile(SessionPath, [FileBase '.h5']);

oldFolder = cd(SessionPath);

% load spike times (Res) and to which cluster each spike belongs (Clu)
[Res, Clu, Map, Par] = LoadCluRes(FileBase);

% to get the channel for theta metrics, get electrode with maximum
% clusters and get its first channel.
Electrodes = unique(Map(:,2));  % existing electrodes
[nClusters, idxMaxClu] = max(histc(Map(:,2), Electrodes));  % index of the electrode with max clusters
Channel = 1 + (Electrodes(idxMaxClu) - 1) * 8;  % Get the first channel

% THETA periods
%thetaPeriods = load([FileBase '.sts.' State]);

% get the running indexes
runningIdxs = h5read([FileBase '.h5'], '/running_idxs');
trajectory = h5read([FileBase '.h5'], '/animal_trajectory').';
sessionDuration = trajectory(length(trajectory), 1) - trajectory(1, 1);

% check running periods:
crit = find(diff(runningIdxs) - 1);

runningPeriods = zeros(ceil(length(crit)/2), 2);
runningPeriods(1, 1) = int64(trajectory(runningIdxs(1) + 1, 8));
runningPeriods(1, 2) = int64(trajectory(runningIdxs(crit(1)), 8));

for i=2:length(crit)
    perStart = int64(trajectory(runningIdxs(crit(i - 1) + 1), 8));
    perEnd = int64(trajectory(runningIdxs(crit(i)), 8));
    runningPeriods(i, 1) = perStart;
    runningPeriods(i, 2) = perEnd;
end

runningSpeedDur = sum(runningPeriods(:, 2) - runningPeriods(:, 1))/Par.SampleRate;

periods = round(runningPeriods*Par.lfpSampleRate/Par.SampleRate);
    
%runningThetaDur = sum(thetaPeriods(:, 2) - thetaPeriods(:, 1))/Par.SampleRate;

%if (runningThetaDur/runningSpeedDur < 0.7)
%    fprintf('WARNING: Theta periods are significantly shorter than actual running duration.\n');
%end
if (runningSpeedDur/sessionDuration < 0.3)
    error('Running periods are significantly shorter than session duration.\n');
end

% always take RUNNING periods for consistency
%if (runningThetaDur > runningSpeedDur)
%   periods = thetaPeriods;
%else
%    periods = runningPeriods;
%end

% perform just test of theta/velocity correlation
%justTest = 1;
%if justTest > 0
%    return
%end

% -----------------------------------------
% load theta phase
ThetaPhase = h5read(['theta.ch' num2str(Channel) '.h5'], '/ThPh');

% resample spike times to the LFP sampling rate
ResLfp = round(Res*Par.lfpSampleRate/Par.SampleRate);

% indices of spikes when animal was running - taking THETA periods
RunIdxs = logical(WithinRanges(ResLfp, periods));

% compute theta phase modulation for each cluster
for i = 1:length(unique(Clu))
    myCluInd = ismember(Clu, i);
    myResLfp = ResLfp(myCluInd & RunIdxs);
    myResLfpALL = ResLfp(myCluInd);  % all spikes for that cluster
    CluThPh = ThetaPhase(myResLfp).';

    if (isempty(CluThPh))
        fprintf('Wrong cluster: %s %s\n', num2str(Map(i, 2)), num2str(Map(i, 3)));
    end
    
    % compute statistics
    rt = RayleighTest(CluThPh, ones(length(CluThPh), 1));
    ppc = PPC(CluThPh);

    % save to the session H5 file
    Electrode = num2str(Map(i, 2));
    Unit = num2str(Map(i, 3));
    DSPath = ['/units/' Electrode '_' Unit '/theta_mod'];
    try
        h5create(h5path, DSPath, size(ThetaPhase(myResLfpALL)));
    catch
    end
    %h5write(h5path, DSPath, CluThPh.');
    h5write(h5path, DSPath, ThetaPhase(myResLfpALL));

    h5writeatt(h5path, DSPath, 'p', rt.p);
    h5writeatt(h5path, DSPath, 'r', rt.r);
    h5writeatt(h5path, DSPath, 'k', rt.k);
    h5writeatt(h5path, DSPath, 'n', rt.n);
    h5writeatt(h5path, DSPath, 'th0', rt.th0);
    h5writeatt(h5path, DSPath, 'logZ', rt.logZ);
    h5writeatt(h5path, DSPath, 'ppc', ppc);
end

cd(oldFolder);
end