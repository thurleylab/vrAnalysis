import os
import h5py
import numpy as np
from matplotlib import cm
import matplotlib.pyplot as plt


def get_corrected_HD_angle(session_path, filebase, canvas=[]):  # canvas=[ax1, ax2, ax3] axes to plot on
    h5path = os.path.join(session_path, '%s.h5' % filebase)
    with h5py.File(h5path, 'r') as f:
        traj = np.array(f['animal_trajectory'])
        velocity = np.array(f['velocity'])

    # select periods with high speed running
    periods = []
    velocity_mod = np.array(velocity)
    velocity_mod[:, 1] -= 0.3  # speed threshold

    for i, speed in enumerate(velocity_mod[:-1]):
        if velocity_mod[i][1] * velocity_mod[i + 1][1] < 0:
            periods.append(i)

    if velocity_mod[periods[0]][1] > 0:
        periods.pop(0)
    if len(periods) % 2 > 0:
        periods.append(len(velocity_mod) - 1)

    high_speed_periods = np.array(periods)  # a 1D array of pairs, like [beg1, end1, beg2, end2, ..]

    # for each long period compute running HD angle and compare with recorded
    ctr = 0
    results = []  # each element: corr coeff between HD recorded/predicted, mean angle difference
    colors = plt.rcParams["axes.prop_cycle"]()
    for i in range(int(len(high_speed_periods)/2)):
        idx_left, idx_right = high_speed_periods[i*2], high_speed_periods[i*2 + 1]
        if idx_right - idx_left > 100:  # take periods > roughly one second
            avg_by = 25
            head_direction = []
            for i, position in enumerate(traj[idx_left:idx_right - avg_by]):
                vectors = [np.array([x[1], x[2]]) - np.array([position[1], position[2]]) for x in traj[idx_left + i + 1:idx_left + i + avg_by]]
                avg_direction = np.array(vectors).sum(axis=0) / len(vectors)

                avg_angle = np.degrees(np.arctan2(avg_direction[1], avg_direction[0]))
                head_direction.append(avg_angle)

            hd_recorded = -traj[idx_left:idx_right - avg_by][:, 5]  # recorded by Optitrack
            hd_computed = np.array(head_direction)                  # predicted by running
            corr = np.corrcoef(hd_computed, np.mod(hd_computed - hd_recorded, 360))
            mean_diff = np.mod(hd_recorded - hd_computed, 360).mean()
            results.append([corr[0][1], mean_diff])

            if not len(canvas) > 0:
                continue

            color=next(colors)["color"]
            canvas[0].scatter(traj[idx_left:idx_right][:, 1], traj[idx_left:idx_right][:, 2], s=5, color=color)
            canvas[1].scatter(traj[idx_left:idx_right - avg_by][:, 0] - traj[idx_left][0], hd_recorded, s=5, color=color)
            canvas[2].scatter(traj[idx_left:idx_right - avg_by][:, 0] - traj[idx_left][0], hd_computed, s=5, color=color)

    results = np.array(results)
    # mean angle delta, weighted average angle delta, SD of estimation, number of samples
    return np.mean(results[:, 1]), np.average(results[:, 1], weights=results[:, 0]), np.std(results[:, 1]), len(results)
