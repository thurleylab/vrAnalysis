function a = firingMetrics(sessionPath, h5name, epochs)
%% computes spatial information content (SIC) and coherence (COH).
% sessionPath  - full path to the session folder where the .h5 file with
% spiketrains is located.
% epochs       - period names, like {'A', 'B', 'Ad', 'Bd'}

% dependency on IO
addpath(genpath('/storage2/andrey/code/labbox'));

h5path = fullfile(sessionPath, h5name);
unitInfo = h5info(h5path, '/units/');

for groupIdx=1:length(unitInfo.Groups)  % iterate over units
    unitPath = [unitInfo.Groups(groupIdx).Name '/'];

    for cIdx=1:numel(epochs)
        placeField = h5read(h5path, [unitPath epochs{cIdx} '/' '04_firing_rate_map']);
        occupField = h5read(h5path, [unitPath epochs{cIdx} '/' '01_occupancy']);

        %% compute spatial information content and coherence
        sic = spatinform(placeField, occupField);
        coh = spatcoherence(placeField);

        h5writeatt(h5path, [unitPath epochs{cIdx} '/' '04_firing_rate_map'], 'information_content', sic);
        h5writeatt(h5path, [unitPath epochs{cIdx} '/' '04_firing_rate_map'], 'spatial_coherence', coh);
    end
end

a = 0;