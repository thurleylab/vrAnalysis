function a = unitMetrics(sessionPath, h5name)

% dependency on IO
addpath(genpath('/storage2/andrey/code/labbox'));
addpath(genpath('/storage2/andrey/code/unit_toolbox'));


%% optionally save data to HDF5
%sessionsPath = '/storage2/andrey/data/processed/00910/';
%files = dir(sessionsPath);

%dirFlags = [files.isdir] & ~strcmp({files.name},'.') & ~strcmp({files.name},'..');
%sessionIds = files(dirFlags);

%for i=1:numel(sessionIds)
%     sessionPath = fullfile(sessionIds(i).folder, sessionIds(i).name);
%     
%     h5files = dir(char(fullfile(sessionPath, '*.h5')));
%     if length(h5files) < 1
%         continue
%     end

fileBase = 'all';
h5path = fullfile(sessionPath, h5name);
%h5path = fullfile(sessionPath, ['all' '.h5']);
    
%% loading unit data from .clu and .res files
[clu, res, spk, map, par] = LoadCluResSpk(fullfile(sessionPath, fileBase));

%% Loading - getting spiketrain, setting electrode / unit number

% available electrode groups and their units (clusters)
for j=1:size(map, 1)
    electrodeGroupNo = map(j, 2);
    unitNo = map(j, 3);

    % getting spiketrain by groupNo / unitNo
    unitIdx = map((map(:, 2) == electrodeGroupNo) & (map(:, 3) == unitNo));
    spikeTrain = res((clu == unitIdx));

    % fprintf('Processing el: %i unit: %i (%i)\n', electrodeGroupNo, unitNo, unitIdx);

    %% 1. Mean Firing Rate
    meanFiringRate = meanfiringrate(spikeTrain, par.SampleRate);


    %% 2. Inter-spike intervals
    times = res(find(clu == unitIdx)) / par.SampleRate;

    %[hist, edges, ISI] = isihist(times, 1, 150, 'type', 'norm', 'scale', 'linear');

    % plot the ISI histogram
    % bar(hist)


    %% 3. Cross- and Auto- correlograms

    binSize = 30;
    halfBins = 30;

    % FROM labbox/TF/CCG.m
    [crossCorr, t, Pairs] = CCG(res, clu, binSize, halfBins, par.SampleRate);

    % plot the autocorrelogram for unit 1
    % bar(crossCorr(:, 1, 1))


    %% 4. Bursting index
    BI = burstindex(times, 20);  % TODO check if 20ms threshold is fine


    %% 5. Isolation distance

    % for cell whose quality is to be evaluated (electrodeGroupNo / unitIdx)
    [Fet, nFeatures] = LoadFet(fullfile(sessionPath, [fileBase '.fet.' num2str(electrodeGroupNo)]));
    [cluFull, nClusters] = LoadClu([fullfile(sessionPath, fileBase) '.clu.' num2str(electrodeGroupNo)]);

    % FROM labbox/Physiology/IsolationDistance.m
    isolDist = IsolationDistance(Fet, find(cluFull==unitNo));


    %% 6. Mahalanobis distances and L-ratio

    % FROM labbox/Physiology/L_ratio.m
    [L, Lratio, df] = L_Ratio(Fet, find(cluFull==unitNo));


    %% output to HDF5
    unitPath = ['/units/' int2str(electrodeGroupNo) '_' int2str(unitNo) '/'];
    h5writeatt(h5path, unitPath, 'mean_firing_rate', meanFiringRate);
    h5writeatt(h5path, unitPath, 'burst_index', BI);
    h5writeatt(h5path, unitPath, 'isolation_distance', isolDist);
    h5writeatt(h5path, unitPath, 'L_ratio', Lratio);
end

a = 0;

% fprintf('Unit metrics for session %s completed.\n', sessionPath);









