import os, sys
import h5py
import numpy as np

from .epochs import get_epochs
from ..adapters.hdf5 import H5NAMES


def get_tpp(session_path, electrode, unit, condition):
    """ compute theta phase precession in 2D egocentric frame """

    # loading animal trajectory
    h5path = os.path.join(session_path, '%s.h5' % H5NAMES.filebase)
    with h5py.File(h5path, 'r') as f:
        traj = np.array(f['animal_trajectory'])
        running_idxs = np.array(f['running_idxs'])
        angle_delta = int(f[H5NAMES.hd].attrs[H5NAMES.hd_angle_delta_weight_avg])

    # loading unit fields data
    h5path = os.path.join(session_path, '%s.h5' % H5NAMES.filebase)
    unit_name = '%s_%s' % (electrode, unit)
    with h5py.File(h5path, 'r') as f:
        if not 'theta_mod' in f['units'][unit_name]:
            return None
        if not H5NAMES.field_clusters in f['units'][unit_name][condition]:
            return None
        condition_idxs = np.array(f[condition + '_idxs'])
        spike_idxs = np.array(f['units'][unit_name]['spike_idxs'])
        theta_mod = np.array(f['units'][unit_name]['theta_mod'])
        field_clu = np.array(f['units'][unit_name][condition][H5NAMES.field_clusters])

    # get place field location data
    sort_idxs = np.argsort(field_clu[:, 7])[::-1]  # sort by number of points in the cluster
    field_clu = field_clu[sort_idxs]  # first two are the best

    # filter spike idxs (running and exp condition)
    spike_idxs = np.vstack([np.arange(len(spike_idxs)), spike_idxs]).T
    mask1 = np.isin(spike_idxs[:, 1], condition_idxs)
    mask2 = np.isin(spike_idxs[:, 1], running_idxs)
    spike_cond_idxs = spike_idxs[mask1 & mask2]

    # compute Xe, Ye - egocentric positions relative to the place field for every spike
    collected = []
    for field_record in field_clu[:2]:
        F = field_record[1:3]  # center of the place field in allocentric

        for spike_no, spike_idx in spike_cond_idxs:
            A = traj[spike_idx][1:3]  # animal position at spike time in allocentric frame
            HD_angle = 360 - (((traj[spike_idx][5]) + angle_delta) % 360)  # head direction angle at spike time in allocentric frame, deg

            FA = F - A  # F vector in egocentric

            R = np.linalg.norm(FA)  # distance from animal to the place field
            phi = (np.degrees(np.arctan2(FA[1], FA[0])) - HD_angle) % 360  # angle to place field in egocentric frame

            phi_rad = np.deg2rad(phi)  # back to radians
            Fe = np.array([R * np.cos(phi_rad), R * np.sin(phi_rad)])  # center of the place field in the egocentric frame

            th0 = float(theta_mod[spike_no])  # theta phase for this spike

            collected.append([field_record[0], spike_idx, HD_angle, Fe[0], Fe[1], R, phi, th0])

    # field id, spike_idx, HD_angle, PFx, PFy, R, phi, theta phase
    return np.array(collected)


def compute_and_save_tpp(where):

    def save_tpp(h5path, unit_name, condition, tpp):
        with h5py.File(h5path, 'r+') as f:
            cond_group = f['units'][unit_name][condition]
            if H5NAMES.tpp in cond_group:
                del cond_group[H5NAMES.tpp]
            cond_group.create_dataset(H5NAMES.tpp, data=tpp)

            for i, name in enumerate(['field_id', 'spike_idx', 'HD_angle', 'PFe_x', 'PFe_y', 'R', 'phi', 'theta_phase']):
                cond_group[H5NAMES.tpp].attrs[name] = i

    conditions = get_epochs(where)

    h5path = os.path.join(where, '%s.h5' % H5NAMES.filebase)
    with h5py.File(h5path, 'r') as f:
        unit_names = [name for name in f['units']]

    for unit_name in unit_names:
        electrode = unit_name.split('_')[0]
        unit = unit_name.split('_')[1]

        for condition in conditions:
            tpp = get_tpp(where, electrode, unit, condition)
            if tpp is not None:
                save_tpp(h5path, unit_name, condition, tpp)
            else:
                print('Skipping %s %s %s: no moduation data' % (where.split('/')[-1], unit_name, condition))
