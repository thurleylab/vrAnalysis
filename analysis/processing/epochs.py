import numpy as np
import h5py
import os

from .utils import get_params_from_json, derive_session_type


class EpochSplitter(object):
    """
    Singleton having methods to split the session timeline into
    relevant periods (arena position, gain, light/dark etc.)

    Each method returns a dict of *absolute* periods like
    {
        'A': [[a0_start, a0_end], [a1_start, a1_end], ..., [an_start, an_end]],
        'B': [[b0_start, b0_end], [b1_start, b1_end], ..., [bn_start, bn_end]],
        ...
        'D': [[d0_start, d0_end]],
        ...
    }
    """

    @staticmethod
    def _get_periods(trajectory, t_start):
        traj = trajectory[:, 0] - t_start
        periods = []

        ar_diffs = np.diff(traj)
        arena_periods = np.where((ar_diffs > 3))[0]

        if len(arena_periods) < 1:  # just a single period
            return [(traj[0], traj[-1])]

        for i, idx in enumerate(arena_periods):
            if i == 0:
                pair = (traj[0], traj[idx])

            else:
                pair = (traj[arena_periods[i - 1] + 1], traj[idx])

            periods.append(pair)

        # add the last period
        if traj[-1] - traj[arena_periods[-1]] > 2:  # more than 2 secs
            periods.append((traj[arena_periods[-1] + 1], traj[-1]))

        return periods

    @staticmethod
    def get_30_physical(session_path):
        """
        Both 'locked' or 'non-locked' conditions should work.

        :param session_path: path to the folder with session files.
        :return:
        """
        h5file = os.path.join(session_path, 'all.h5')

        with h5py.File(h5file, 'r') as f:
            arena_trajectory = np.array(f['arena_trajectory'])

        config = get_params_from_json(session_path)['vr']['scene']

        dark_start = 10 ** 5
        if 'timers' in config and 'turn_projection_off' in config['timers']:
            dark_start = config['timers']['turn_projection_off']

        light_indexes = np.where((arena_trajectory[:, 0] - arena_trajectory[0][0] < dark_start))[0]
        dark_indexes = np.where((arena_trajectory[:, 0] - arena_trajectory[0][0] > dark_start))[0]

        arena_max = arena_trajectory[:, 2].max()
        arena_min = arena_trajectory[:, 2].min()

        ar_traj_A_idx = np.intersect1d(light_indexes, np.where((arena_trajectory[:, 2] < arena_min + 0.03))[0])
        ar_traj_B_idx = np.intersect1d(light_indexes, np.where((arena_trajectory[:, 2] > arena_max - 0.03))[0])
        ar_traj_Ad_idx = np.intersect1d(dark_indexes, np.where((arena_trajectory[:, 2] < arena_min + 0.03))[0])
        ar_traj_Bd_idx = np.intersect1d(dark_indexes, np.where((arena_trajectory[:, 2] > arena_max - 0.03))[0])

        t_start = arena_trajectory[0][0]
        periods = {
            'A': EpochSplitter._get_periods(arena_trajectory[ar_traj_A_idx], t_start),
            'B': EpochSplitter._get_periods(arena_trajectory[ar_traj_B_idx], t_start)
        }

        if len(dark_indexes) > 5:  # there is a dark period in this session
            periods['Ad'] = EpochSplitter._get_periods(arena_trajectory[ar_traj_Ad_idx], t_start)
            periods['Bd'] = EpochSplitter._get_periods(arena_trajectory[ar_traj_Bd_idx], t_start)

        return periods


    @staticmethod
    def get_single(session_path):
        config = get_params_from_json(session_path)['vr']['scene']

        if 'move_freq' in config:
            half = config['timers'][1]
            return {
                'A': [[0, half]],
                'B': [[half, 2 * half]]
            }

        full = config['move_interval']
        half = full / 2

        return {
            'A': [[0, full]],
            'B': [[full, 2 * full]],
            #'B1': [[full, full + half]],
            #'B2': [[full + half, 2 * full]],
            'Ad': [[2 * full, 2 * full + half]],
            'Bd': [[2 * full + half, 3 * full]]
        }


    @staticmethod
    def get_30_visual(session_path):
        h5file = os.path.join(session_path, 'all.h5')

        with h5py.File(h5file, 'r') as f:
            arena_trajectory = np.array(f['arena_trajectory'])

        config = get_params_from_json(session_path)['vr']['scene']

        dark_start = 10 ** 5
        if 'timers' in config and 'turn_projection_off' in config['timers']:
            dark_start = config['timers']['turn_projection_off']

        t_start = arena_trajectory[0][0]
        traj_norm = arena_trajectory[:, 0] - t_start

        light_indexes = np.where((traj_norm < dark_start))[0]
        dark_indexes = np.where((traj_norm > dark_start))[0]

        ar_traj_A_idx = np.intersect1d(light_indexes, np.where((traj_norm % 60 > 5) & (traj_norm % 60 < 30))[0])
        ar_traj_B_idx = np.intersect1d(light_indexes, np.where((traj_norm % 60 > 35) & (traj_norm % 60 < 60))[0])
        #ar_traj_Ad_idx = np.intersect1d(dark_indexes, np.where((traj_norm % 60 > 5) & (traj_norm % 60 < 30))[0])
        #ar_traj_Bd_idx = np.intersect1d(dark_indexes, np.where((traj_norm % 60 > 35) & (traj_norm % 60 < 60))[0])

        periods = {
            'A': EpochSplitter._get_periods(arena_trajectory[ar_traj_A_idx], t_start),
            'B': EpochSplitter._get_periods(arena_trajectory[ar_traj_B_idx], t_start)
        }

        if len(dark_indexes) > 5:  # there is a dark period in this session
            periods['D'] = [[dark_start, traj_norm[-1]]]

        return periods

    @staticmethod
    def get_VFG(session_path):
        config = get_params_from_json(session_path)['vr']['scene']

        if 'transition_duration' in config:   # new VFG experiments
            t_switch = config['gain_switch_interval']
            t_trans = config['transition_duration']

            periods = {
                'A': [(0, t_switch)],
                #'AB': [(t_switch, t_switch + t_trans)],
                'B': [(t_switch + t_trans, 2 * t_switch + t_trans)],
                #'BC': [(2 * t_switch + t_trans, 2 * t_switch + 2 * t_trans)],
                'C': [(2 * t_switch + 2 * t_trans, 3 * t_switch + 2 * t_trans)],
            }

            # EXCEPTIONS!
            if session_path.find('2020-10-15_22-04-26') > 0:  # gerbil ate the cable
                return periods

            if 'timers' in config and 'turn_projection_off' in config['timers']:
                dark_start = config['timers']['turn_projection_off']
                t_end = config['timers']['stop_experiment']

                periods['D'] = [(3 * t_switch + 2 * t_trans, t_end)]

            return periods

        else:   # old VFG experiments
            t_switch = 1.0/config['gain_switch_freq'] if 'gain_switch_freq' in config else config['gain_switch_interval']
            t_end = float(config['timers'][0])

            periods_number = int(round(t_end/t_switch))
            period_duration = int(round(t_end/periods_number))

            periods = {
                'A': [(2 * i * period_duration, 2 * i * period_duration + period_duration) for i in
                      range(int(periods_number/2))],
                'B': [(2 * i * period_duration + period_duration, 2 * i * period_duration + 2 * period_duration) for i in
                      range(int(periods_number / 2))]
            }

            return periods


def get_epochs(session_path):
    session_type = derive_session_type(session_path)
    selector = {
        'GAIN': lambda x: EpochSplitter.get_VFG(x),
        'SHIFT periodic': lambda x: EpochSplitter.get_30_physical(x),
        'SHIFT visual': lambda x: EpochSplitter.get_30_visual(x),
        'SHIFT both': lambda x: EpochSplitter.get_30_physical(x),
        'SHIFT single': lambda x: EpochSplitter.get_single(x)
    }

    epochs = None
    for type_name, func in selector.items():
        if session_type.find(type_name) > -1:
            epochs = func(session_path)
            break

    if not epochs:
        raise ValueError("Can't define type and time periods for %s" % session_path[-20:])

    return epochs
