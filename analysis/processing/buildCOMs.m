function a = buildCOMs(sessionPath, epochs)
%% computes Center of Masses (COMs) for a given session.
% sessionPath  - full path to the session folder where the .h5 file with
% spiketrains is located.
% epochs       - period names, like {'A', 'B', 'Ad', 'Bd'}


% dependency on IO
%rmpath(genpath('/storage/share/matlab/labbox'));
addpath(genpath('/storage2/andrey/code/labbox'));


%% for many sessions - SHIFT Physical 30
sessionsList = {
 {'003281', '2019-10-22_10-12-05'};
 {'003281', '2019-10-21_22-25-20'};
 {'003281', '2019-10-23_15-32-59'};
 {'003281', '2019-10-24_17-48-14'};
 {'003281', '2019-10-23_22-25-54'};
 {'003281', '2019-10-29_13-13-13'};
 {'003281', '2019-10-29_20-15-08'};
 {'003281', '2019-10-30_14-39-20'};
 {'003281', '2019-10-31_09-12-48'};
 {'003281', '2019-11-01_11-18-23'};
 {'003281', '2019-11-01_18-45-22'};
 {'003281', '2019-11-02_13-38-55'};
 {'003281', '2019-11-05_20-11-18'};
 {'003281', '2019-11-07_11-51-32'};
 {'003281', '2019-11-19_08-48-35'};
 {'003282', '2019-10-24_13-35-20'};
 {'003282', '2019-10-23_23-00-59'};
 {'003282', '2019-10-24_21-20-25'};
 {'003282', '2019-11-04_09-30-03'};
 {'003282', '2019-11-05_10-54-13'};
 {'003282', '2019-11-05_20-31-59'};
 {'003282', '2019-11-06_22-36-07'};
 {'003282', '2019-11-12_10-07-21'};
 {'003282', '2019-11-15_10-53-53'};
 {'003282', '2019-11-18_10-35-43'};
 {'00908', '2019-07-09_08-49-24'};
 {'00908', '2019-07-08_22-15-41'};
 {'00908', '2019-07-08_17-46-20'};
 {'00908', '2019-07-09_21-59-47'};
 {'00908', '2019-07-09_17-00-25'};
 {'00908', '2019-07-12_17-32-40'};
 {'00908', '2019-07-11_13-06-27'};
 {'00908', '2019-07-11_08-24-01'};
 {'00908', '2019-07-10_22-26-42'};
 {'00908', '2019-07-10_17-44-31'};
 {'00908', '2019-07-17_21-49-55'};
 {'00908', '2019-07-22_10-04-13'};
 {'00908', '2019-07-21_17-29-37'};
 {'00908', '2019-07-18_14-27-52'};
 {'00908', '2019-07-22_22-25-02'};
 {'00908', '2019-07-24_11-17-34'};
 {'00908', '2019-07-16_18-30-55'};
 {'00910', '2019-07-22_09-46-16'};
 {'00910', '2019-07-21_17-01-34'};
 {'00910', '2019-07-22_22-05-31'};
 {'00910', '2019-07-23_22-01-29'};
 {'00910', '2019-07-25_09-46-22'}
};


%% for many sessions - SHIFT Physical AWAY
sessionsList = {
 {'00908', '2019-07-23_22-28-17'};
 {'00908', '2019-07-15_16-10-53'};
 {'00908', '2019-07-13_14-30-15'};
 {'00908', '2019-07-14_15-42-00'};
 {'00908', '2019-07-15_22-28-43'};
 {'00910', '2019-07-24_10-59-13'};
 {'00910', '2019-07-25_20-54-31'};
 {'00910', '2019-08-07_09-30-56'}
};

%% for many sessions - SHIFT visual
sessionsList = {
 {'003281', '2019-11-18_11-10-48'};
 {'003282', '2019-11-14_13-27-17'};
 {'003282', '2019-11-15_09-29-33'};
 {'003282', '2019-11-18_22-45-10'};
 {'003282', '2019-11-28_09-45-34'};
 {'003282', '2019-11-29_22-35-08'};
 {'003282', '2019-12-04_09-26-29'}
};


%% for many sessions - SHIFT Physical 30 +dark
sessionsList = {
 {'003281', '2019-11-21_13-44-47'};
 {'003281', '2019-11-21_22-51-03'};
 {'003282', '2019-11-20_15-58-39'};
 {'003282', '2019-11-21_09-44-05'};
 {'003282', '2019-11-21_21-58-48'};
 {'003282', '2019-11-24_20-06-11'};
 {'003282', '2019-11-26_09-48-18'};
 {'003282', '2019-11-27_17-12-57'};
 {'003282', '2019-11-28_13-31-13'};
 {'003282', '2019-11-29_14-16-57'};
 {'003282', '2019-12-02_17-51-29'}
};


%% for many sessions - SHIFT both / SHIFT both +dark
sessionsList = {
 {'003282', '2019-11-18_21-48-32'};
 {'003281', '2019-11-18_22-10-40'};
 {'003282', '2019-12-03_10-36-38'};
 {'003282', '2019-11-28_15-28-59'};
 {'003282', '2019-12-05_10-04-14'}
};


%% for many sessions - dark periods
sessionsList = {
 {'003282', '2019-11-21_09-44-05'};
 {'003282', '2019-11-29_14-16-57'};
 {'003281', '2019-11-21_13-44-47'};
 {'003282', '2019-11-21_21-58-48'};
 {'003281', '2019-11-21_22-51-03'};
 {'003282', '2019-11-20_15-58-39'};
 {'003282', '2019-11-27_17-12-57'};
 {'003282', '2019-11-28_13-31-13'};
 {'003282', '2019-12-02_17-51-29'};
 {'003282', '2019-11-24_20-06-11'};
 {'003282', '2019-11-26_09-48-18'}
};


%% for many sessions - GAIN sessions
sessionsList = {
 {'003282', '2019-11-12_21-23-37'};
 {'003282', '2019-11-12_15-29-13'};
 {'003281', '2019-11-17_15-34-04'};
 {'003281', '2019-11-07_21-41-36'};
 {'003282', '2019-11-08_17-35-28'};
 {'003282', '2019-11-13_12-54-37'};
 {'003282', '2019-11-07_22-05-39'};
 {'003282', '2019-11-22_10-16-28'};
 {'003281', '2019-11-20_15-15-05'};
 {'003282', '2019-11-13_22-18-19'};
 {'003282', '2019-12-03_19-51-34'};
 {'003282', '2019-11-26_21-58-57'};
 {'003282', '2019-11-14_20-47-14'};
 {'003282', '2019-11-17_15-59-58'};
 {'003282', '2019-11-20_11-41-38'};
 {'003282', '2019-11-19_08-18-21'};
 {'003282', '2019-11-11_13-04-49'};
 {'003282', '2019-11-12_20-24-17'}
};


%% for many sessions - ABBA sessions
sessionsList = {
 {'003282', '2019-11-26_14-52-16'};
 {'00910', '2019-08-07_11-01-52'};
 {'00910', '2019-08-15_12-33-23'};
 {'003282', '2019-11-30_13-51-14'};
 {'00910', '2019-08-06_16-58-32'};
 {'00910', '2019-08-13_11-09-22'};
 {'00910', '2019-08-08_18-42-52'};
 {'00910', '2019-08-06_11-24-47'};
 {'00910', '2019-08-09_13-36-37'};
 {'00908', '2019-07-24_20-31-26'};
 {'00910', '2019-08-09_16-35-22'};
 {'00910', '2019-08-12_17-16-34'};
 {'00910', '2019-08-07_16-16-13'};
 {'00910', '2019-08-05_14-08-23'};
 {'003282', '2019-11-27_22-17-07'};
 {'00910', '2019-08-13_17-31-03'};
 {'00910', '2019-08-08_10-13-38'}
};

%% for a single session
%sessionsList = {{'003282', '2019-11-26_14-52-16'}};


%% place field processing
%sessionPath = '/storage2/andrey/data/processed';
%conditions = {'AB', 'BA'};

%for sIdx=1:length(sessionsList)  % iterate over sessions
%    animal = sessionsList{sIdx}{1};
%    sessionId = sessionsList{sIdx}{2};

h5path = fullfile(sessionPath, ['all' '.h5']);
unitInfo = h5info(h5path, '/units/');

for groupIdx=1:length(unitInfo.Groups)  % iterate over units
    unitPath = [unitInfo.Groups(groupIdx).Name '/'];
    xy_range = h5readatt(h5path, unitPath, 'xy_range');

    for cIdx=1:numel(epochs)
        placeField = h5read(h5path, [unitPath epochs{cIdx} '_place_field']);
        occupField = h5read(h5path, [unitPath epochs{cIdx} '_occupancy']);

        %% compute spatial information content and coherence
        sic = spatinform(placeField, occupField);
        coh = spatcoherence(placeField);

        h5writeatt(h5path, [unitPath epochs{cIdx} '_place_field'], 'information_content', sic);
        h5writeatt(h5path, [unitPath epochs{cIdx} '_place_field'], 'spatial_coherence', coh);

        %% compute number of fields and patch coordinates
        [pfield, mapI] = placefield2D(placeField, 20, 0.5 * max(placeField(:)));
        if ~isfield(mapI, 'selected')
            selected = zeros(size(mapI.binary));
        else
            selected = mapI.selected;
        end
        
        %selected = placefield2DMAX(placeField, 0.2 * max(placeField(:)), 20, 2);

        % X and Y positions as matrixes
        [y_size, x_size] = size(placeField);  % numbers of bins in X, Y
        x_lin = linspace(xy_range(1), xy_range(2), x_size);
        x_pos = repelem(x_lin, y_size, 1);

        y_lin = linspace(xy_range(3), xy_range(4), y_size);
        y_pos = repelem(y_lin.', 1, x_size);

        %% compute COMs
        nFields = length(unique(selected)) - 1;  % number of place fields
        COM = zeros(nFields, 4);

        if nFields > 0
            for i=1:nFields
                fA = selected == i;  % linear indexes of bins inside the field
                fRate = placeField(fA);  % field firing only for this field

                COMx = sum(fRate .* x_pos(fA)) / sum(fRate);
                COMy = sum(fRate .* y_pos(fA)) / sum(fRate);

                COM(i,:) = [i, COMx, COMy, max(fRate)];
            end
        end

        %% save COMs to H5
        if ~isempty(COM)
            COMPath = [unitPath epochs{cIdx} '_fields_COMs'];
            try
                h5create(h5path, COMPath, size(COM.'));
            catch
            end
            h5write(h5path, COMPath, COM.');
        end

        %% save place fields (patches)
        fieldsPath = [unitPath epochs{cIdx} '_fields'];
        try
            h5create(h5path, fieldsPath, size(selected));
        catch
        end
        h5write(h5path, fieldsPath, selected);

    end
end

a = 0;
%disp([num2str(length(sessionsList) - sIdx) ' left']);
%end


%% ------------- Figure for validation ---------------------- %
% figure;
% subplot(1,4,1); imagesc(flipud(placeFieldA));colorbar;
% subplot(1,4,2); imagesc(flipud(map.binary));colorbar;
% subplot(1,4,3); imagesc(flipud(map.con));colorbar;
% subplot(1,4,4); imagesc(flipud(map.selected));colorbar;
