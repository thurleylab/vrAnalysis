function a = thetaMetrics(sessionPath, fileBase)

% dependency on IO
addpath(genpath('/storage2/andrey/code/labbox'));

currDir = pwd;
cd(sessionPath);

% to get the channel for computing theta metrics, get electrode with maximum
% clusters and get its first channel.

[T,G,Map,Par]=LoadCluRes(fileBase);  % load all clusters
Electrodes = unique(Map(:,2));  % get existing electrodes
[nClusters, idxMaxClu] = max(histc(Map(:,2), Electrodes));  % compute the index of the electrode with max clusters
Channel = 1 + (Electrodes(idxMaxClu) - 1) * 8;  % Get the first channel

% Compute theta metrics
ThetaParams(fileBase, Channel);

a = 0;

cd(currDir);
%fprintf('Theta metrics (Ch: %s) for session %s completed.\n', num2str(Channel), sessionPath);
