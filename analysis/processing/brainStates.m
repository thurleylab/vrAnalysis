function out = brainStates(SessionPath, FileBase)
% brain state detection

% dependencies on toolboxes
addpath(genpath('/storage/share/matlab/labbox'));
addpath(genpath('/storage/share/matlab/Third-Party_Toolboxes/FMAToolbox'))

oldFolder = cd(SessionPath);

% load spike times (Res) and to which cluster each spike belongs (Clu)
[Res, Clu, Map, Par] = LoadCluRes(FileBase);

% to get the channel for theta metrics, get electrode with maximum
% clusters and get its first channel.
Electrodes = unique(Map(:,2));  % existing electrodes
[nClusters, idxMaxClu] = max(histc(Map(:,2), Electrodes));  % index of the electrode with max clusters
Channel = 1 + (Electrodes(idxMaxClu) - 1) * 8;  % Get the first channel

% detect brain states (RUN / not RUN)
DetectBrainStates(FileBase, 'lfp', Channel, 5, 'none');

cd(oldFolder);
out = 0;

end