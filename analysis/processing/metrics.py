import h5py
import os
import numpy as np
import pandas as pd

from ..adapters.hdf5 import H5NAMES
from .epochs import get_epochs


def mapstat(nmap, posPDF):
    '''
    Copy of Christian's Matlab code.  His seems to work with matrices of place fields, but this one only works with 1D versions

    nmap: bins of spike data by position (should not be normalized first)
    posPDF: probability density function of position data (must be normalized first)

    More testing can be done but seems to be working properly on 11.04.2019

    '''
    meanrate = np.nansum(np.nansum(np.multiply(nmap, posPDF)))
    meansquarerate = np.nansum(np.nansum(np.multiply(nmap ** 2, posPDF)))

    if meansquarerate == 0:
        sparsity = float('nan')
    else:
        sparsity = meanrate ** 2 / meansquarerate

    maxrate = np.max(nmap)

    if meanrate == 0:
        selectivity = float('nan')
    else:
        selectivity = maxrate / meanrate

    i1 = np.where((nmap > 0) & (posPDF > 0))[0]
    i2 = np.where((nmap > 0) & (posPDF > 0))[1]

    if len(i1) > 0:
        akksum = 0

        for i in range(len(i1)):
            ii1 = i1[i]
            ii2 = i2[i]
            akksum += posPDF[ii1][ii2] * (nmap[ii1][ii2] / meanrate) * np.log2(nmap[ii1][ii2] / meanrate)

        information = akksum

    else:
        information = float('nan')

    return information, sparsity, selectivity


def mean_firing_rate(spiketrain, sampling_rate=3*10**4):
    if len(spiketrain) < 5:
        return 0
    mean_ISI = np.mean(np.diff(spiketrain))
    return sampling_rate / mean_ISI


def write_sic_and_mfr(session_path, filebase='all'):
    # loading unit groups
    h5file = os.path.join(session_path, '%s.h5' % filebase)
    with h5py.File(h5file, 'r') as f:
        unit_groups = [x for x in f['units']]

    # processing single units
    for unit_group_name in unit_groups:

        epochs = get_epochs(session_path)
        electrode = unit_group_name.split('_')[0]
        unit = unit_group_name.split('_')[1]

        with h5py.File(h5file, 'r+') as f:
            unit_group = f['units']['%s_%s' % (electrode, unit)]
            trajectory = np.array(f['animal_trajectory'])

            for cond in epochs.keys():
                cond_group = unit_group[cond]

                # --------- reading unit occupancy / spiking data ------

                omap = np.array(cond_group['01_occupancy'])
                smap = np.array(cond_group['02_spiking'])

                # ------------- compute spatial metrics ---------------

                information, sparsity, selectivity = mapstat(smap, omap / np.sum(omap))

                cond_idxs = np.array(f[cond + '_idxs'])
                all_spk_idxs = np.array(unit_group['spike_idxs'])

                spiketrain = np.intersect1d(cond_idxs, all_spk_idxs)
                mfr = mean_firing_rate(trajectory[spiketrain][:, 7])

                cond_group['04_firing_rate_map'].attrs['information'] = information
                cond_group['04_firing_rate_map'].attrs['sparsity'] = sparsity
                cond_group['04_firing_rate_map'].attrs['selectivity'] = selectivity
                cond_group['04_firing_rate_map'].attrs['mean_firing_rate'] = mfr
                cond_group['01_occupancy'].attrs['ratio'] = len(np.where(omap > 0)[0]) / omap.size

#
# def get_metrics(source, sessions, exclude_locations=True, nA='A', nB='B'):
#     shifted = []
#     remapped_A = []
#     remapped_B = []
#     disappeared = []
#     appeared = []
#
#     for i, (animal, session_id, s_type) in enumerate(sessions):
#         sessionpath = os.path.join(source, animal, session_id)
#         h5file = os.path.join(sessionpath, 'all.h5')
#         with h5py.File(h5file, 'r') as f:
#             unit_groups = [x for x in f['units']]
#
#         # processing single units
#         for unit_group_name in unit_groups:
#
#             # ------------- reading unit data ---------------
#
#             f = h5py.File(h5file, 'r')
#
#             electrode = unit_group_name.split('_')[0]
#             unit = unit_group_name.split('_')[1]
#
#             unit_group = f['units']['%s_%s' % (electrode, unit)]
#
#             shift_name = 'COMShift'
#             if nA.find('d') > -1:
#                 shift_name = nA + nB + shift_name
#             COMShiftMatrix = np.array(unit_group[shift_name]) if shift_name in unit_group else []
#             isoldist = unit_group.attrs['isolation_distance'][0]
#
#             fieldsA = np.array(unit_group[nA + '_fields'])
#             fieldsB = np.array(unit_group[nB + '_fields'])
#
#             COMa = np.array(unit_group[nA + '_fields_COMs']) if nA + '_fields_COMs' in unit_group else None
#             COMb = np.array(unit_group[nB + '_fields_COMs']) if nB + '_fields_COMs' in unit_group else None
#
#             sicA = unit_group[nA + '_place_field'].attrs['information_content']
#             sicB = unit_group[nB + '_place_field'].attrs['information_content']
#
#             xy_range = np.array(unit_group.attrs['xy_range'])
#
#             f.close()
#
#             # ------------- selecting field pairs = shifted fields ----------
#
#             A_used_fields, B_used_fields = [], []
#
#             if len(COMShiftMatrix) > 0:
#                 for i, field_A_shifts in enumerate(COMShiftMatrix):
#
#                     field_A_id = i + 1  # current field
#                     field_B_id = 1  # start with the first one
#
#                     distance_img = 100  # very large distance
#                     overlap = 0
#
#                     for j, pair_metrics in enumerate(field_A_shifts):
#                         isect_coeff0 = pair_metrics[2] / ((pair_metrics[0] + pair_metrics[1]) / 2)
#                         isect_coeff30 = pair_metrics[3] / ((pair_metrics[0] + pair_metrics[1]) / 2)
#
#                         curr_dist = pair_metrics[4] if isect_coeff0 > isect_coeff30 else pair_metrics[5]
#                         curr_over = isect_coeff0 if isect_coeff0 > isect_coeff30 else isect_coeff30
#
#                         if curr_dist < distance_img:
#                             distance_img = float(curr_dist)
#                             # distance_real = float(pair_metrics[4]) # sqrt( dX**2 + dY**2 )
#                             distance_real = float(pair_metrics[6])  # dY
#                             overlap = float(curr_over)
#                             field_B_id = j + 1
#
#                     fA_size = fieldsA[(fieldsA == field_A_id)].sum()
#                     fB_size = fieldsB[(fieldsB == field_B_id)].sum()
#                     delta_field_size = (fB_size - fA_size) / (fA_size + fB_size)
#
#                     peak_FRa = COMa[COMa[:, 0] == field_A_id][0][3]
#                     peak_FRb = COMb[COMb[:, 0] == field_B_id][0][3]
#
#                     COMa_x = COMa[COMa[:, 0] == field_A_id][0][1]
#                     COMa_y = COMa[COMa[:, 0] == field_A_id][0][2]
#                     COMb_x = COMb[COMb[:, 0] == field_B_id][0][1]
#                     COMb_y = COMb[COMb[:, 0] == field_B_id][0][2]
#
#                     row = [
#                         animal,
#                         session_id,
#                         int(electrode),
#                         int(unit),
#                         field_A_id,
#                         field_B_id,
#                         overlap,
#                         distance_img,
#                         distance_real,
#                         sicA,
#                         sicB,
#                         len(COMa),  # number of fields in A
#                         len(COMb),  # number of fields in B
#                         peak_FRa,  # peak firing rate A
#                         peak_FRb,  # peak firing rate B
#                         delta_field_size,  # relative change in field size
#                         COMa_x,
#                         COMa_y,
#                         COMb_x,
#                         COMb_y,
#                         isoldist
#                     ]
#                     if overlap > 0.0:
#                         shifted.append(row)
#                         A_used_fields.append(field_A_id)
#                         B_used_fields.append(field_B_id)
#
#             # ----------- fields without a pair ----------------
#
#             num_fields_A = len(np.unique(fieldsA)) - 1
#             num_fields_B = len(np.unique(fieldsB)) - 1
#
#             # 1. fields that exist in A but not in B
#             if num_fields_A > 0 and num_fields_B == 0:
#                 for f_id, COMa_x, COMa_y, peak_FRa in COMa:
#                     disappeared.append([
#                         animal, session_id, int(electrode), int(unit), f_id, 0,
#                         0, 0, 0, sicA, 0, len(COMa), 0, peak_FRa, 0, 0, COMa_x,
#                         COMa_y, None, None
#                     ])
#
#             # 2. fields that exist in B but not in A
#             if num_fields_A == 0 and num_fields_B > 0:
#                 for f_id, COMb_x, COMb_y, peak_FRb in COMb:
#                     appeared.append([
#                         animal, session_id, int(electrode), int(unit), 0, f_id,
#                         0, 0, 0, 0, sicB, 0, len(COMb), 0, peak_FRb, 0, None,
#                         None, COMb_x, COMb_y
#                     ])
#
#             # 3. fields that don't have an overlapping pair
#             fields_num_a = len(COMShiftMatrix)
#             for i in range(fields_num_a):
#                 field_A_id = i + 1
#
#                 if field_A_id not in A_used_fields:
#                     peak_FRa = COMa[COMa[:, 0] == field_A_id][0][3]
#                     COMa_x = COMa[COMa[:, 0] == field_A_id][0][1]
#                     COMa_y = COMa[COMa[:, 0] == field_A_id][0][2]
#
#                     remapped_A.append([
#                         animal, session_id, int(electrode), int(unit), field_A_id, 0,
#                         0, 0, 0, sicA, sicB, len(COMa), len(COMb), peak_FRa, 0, 0, COMa_x,
#                         COMa_y, None, None
#                     ])
#
#             fields_num_b = len(COMShiftMatrix[0]) if len(COMShiftMatrix) > 0 else 0
#             for i in range(fields_num_b):
#                 field_B_id = i + 1
#
#                 if field_B_id not in B_used_fields:
#                     peak_FRb = COMb[COMb[:, 0] == field_B_id][0][3]
#                     COMb_x = COMb[COMb[:, 0] == field_B_id][0][1]
#                     COMb_y = COMb[COMb[:, 0] == field_B_id][0][2]
#
#                     remapped_B.append([
#                         animal, session_id, int(electrode), int(unit), 0, field_B_id,
#                         0, 0, 0, sicA, sicB, len(COMa), len(COMb), 0, peak_FRb, 0, None,
#                         None, COMb_x, COMb_y
#                     ])
#
#     print("PF match found for %d fields, %d fields remapped" % (len(shifted), len(remapped_A) + len(remapped_B)))
#
#     if exclude_locations:
#         shifted = np.array([x[2:] for x in shifted])
#         remapped_A = np.array([x[2:] for x in remapped_A])
#         remapped_B = np.array([x[2:] for x in remapped_B])
#         appeared = np.array([x[2:] for x in appeared])
#         disappeared = np.array([x[2:] for x in disappeared])
#
#     return shifted, remapped_A, remapped_B, appeared, disappeared
#
#
# def get_metrics_from_bootstrap(source, sessions, exclude_locations=True, nA='A', nB='B'):
#     shifted = []
#     remapped_A = []
#     remapped_B = []
#     disappeared = []
#     appeared = []
#
#     for i, (animal, session_id, s_type) in enumerate(sessions):
#         sessionpath = os.path.join(source, animal, session_id)
#         h5file = os.path.join(sessionpath, 'all.h5')
#         h5bootfile = os.path.join(sessionpath, 'bootstrap.h5')
#         with h5py.File(h5file, 'r') as f:
#             unit_groups = [x for x in f['units']]
#
#         # processing single units
#         for unit_group_name in unit_groups:
#
#             # ------------- reading unit data ---------------
#
#             f = h5py.File(h5file, 'r')
#
#             electrode = unit_group_name.split('_')[0]
#             unit = unit_group_name.split('_')[1]
#
#             unit_group = f['units']['%s_%s' % (electrode, unit)]
#
#             isoldist = unit_group.attrs['isolation_distance'][0]
#             sicA = unit_group[nA + '_place_field'].attrs['information_content']
#             sicB = unit_group[nB + '_place_field'].attrs['information_content']
#
#             xy_range = np.array(unit_group.attrs['xy_range'])
#
#             f.close()
#
#             with h5py.File(h5bootfile, 'r') as f:
#                 unit_group = f['%s_%s' % (electrode, unit)]
#                 if not nA in unit_group or not nB in unit_group:
#                     continue  # requested epochs were not recorded in this session
#
#                 shift_name = "%s_%s_%s" % (nA, nB, 'COMShift')
#                 COMShiftMatrix = np.array(unit_group[shift_name]) if shift_name in unit_group else []
#
#                 fieldsA = np.array(unit_group[nA]['fields']) if 'fields' in unit_group[nA] else []
#                 fieldsB = np.array(unit_group[nB]['fields']) if 'fields' in unit_group[nA] else []
#
#                 COMa = np.array(unit_group[nA]['clusters']) if 'clusters' in unit_group[nA] else None
#                 COMb = np.array(unit_group[nB]['clusters']) if 'clusters' in unit_group[nB] else None
#
#             # ------------- selecting field pairs = shifted fields ----------
#
#             A_used_fields, B_used_fields = [], []
#
#             if len(COMShiftMatrix) > 0:
#                 for i, field_A_shifts in enumerate(COMShiftMatrix):
#                     #print('Start: %s %s %s' % (session_id, electrode, unit))
#
#                     distance_img = 100  # very large distance
#                     overlap = 0
#
#                     for j, pair_metrics in enumerate(field_A_shifts):
#                         isect_coeff0 = pair_metrics[2] / ((pair_metrics[0] + pair_metrics[1]) / 2)
#                         isect_coeff30 = pair_metrics[3] / ((pair_metrics[0] + pair_metrics[1]) / 2)
#
#                         curr_dist = pair_metrics[4] if isect_coeff0 > isect_coeff30 else pair_metrics[5]
#                         curr_over = isect_coeff0 if isect_coeff0 > isect_coeff30 else isect_coeff30
#
#                         if curr_dist < distance_img:
#                             distance_img = float(curr_dist)
#                             # distance_real = float(pair_metrics[4]) # sqrt( dX**2 + dY**2 )
#                             distance_real = float(pair_metrics[6])  # dY
#                             overlap = float(curr_over)
#                             field_A_id, field_B_id = pair_metrics[7], pair_metrics[8]
#
#                     fA_idxs = np.where((fieldsA == field_A_id))[0]
#                     fB_idxs = np.where((fieldsB == field_B_id))[0]
#                     fA_size = len(fA_idxs)
#                     fB_size = len(fB_idxs)
#                     delta_field_size = (fB_size - fA_size) / (fA_size + fB_size)
#                     vlength_A = (fA_idxs[-1] - fA_idxs[0]) / len(fieldsA)  # if fA_size > 0 else 0
#                     vlength_B = (fB_idxs[-1] - fB_idxs[0]) / len(fieldsB)  # if fB_size > 0 else 0
#
#                     peak_FRa = COMa[COMa[:, 0] == field_A_id][0][3]
#                     peak_FRb = COMb[COMb[:, 0] == field_B_id][0][3]
#
#                     COMa_x = COMa[COMa[:, 0] == field_A_id][0][1]
#                     COMa_y = COMa[COMa[:, 0] == field_A_id][0][2]
#                     COMb_x = COMb[COMb[:, 0] == field_B_id][0][1]
#                     COMb_y = COMb[COMb[:, 0] == field_B_id][0][2]
#
#                     row = [
#                         animal,  # 0
#                         session_id,  # 1
#                         int(electrode),  # 2
#                         int(unit),  # 3
#                         field_A_id,  # 4
#                         field_B_id,  # 5
#                         overlap,  # 6
#                         distance_img,  # 7
#                         distance_real,  # 8
#                         sicA,  # 9
#                         sicB,  # 10
#                         len(COMa),  # 11 number of fields in A
#                         len(COMb),  # 12 number of fields in B
#                         peak_FRa,  # 13 peak firing rate A
#                         peak_FRb,  # 14 peak firing rate B
#                         delta_field_size,  # 15 relative change in field size
#                         COMa_x,  # 16
#                         COMa_y,  # 17
#                         COMb_x,  # 18
#                         COMb_y,  # 19
#                         isoldist,  # 20
#                         fA_size,  # 21 field A size
#                         fB_size,  # 22 field B size
#                         vlength_A,  # 23 field A longitudinal size
#                         vlength_B  # 24 field B longitudinal size
#                     ]
#
#                     if overlap > 0.0:
#                         shifted.append(row)
#                         A_used_fields.append(field_A_id)
#                         B_used_fields.append(field_B_id)
#
#             # ----------- fields without a pair ----------------
#
#             # num_fields_A = len(np.unique(fieldsA)) - 1
#             # num_fields_B = len(np.unique(fieldsB)) - 1
#             #
#             # # 1. fields that exist in A but not in B
#             # if num_fields_A > 0 and num_fields_B == 0:
#             #     for f_id, COMa_x, COMa_y, peak_FRa in COMa:
#             #         disappeared.append([
#             #             animal, session_id, int(electrode), int(unit), f_id, 0,
#             #             0, 0, 0, sicA, 0, len(COMa), 0, peak_FRa, 0, 0, COMa_x,
#             #             COMa_y, None, None
#             #         ])
#             #
#             # # 2. fields that exist in B but not in A
#             # if num_fields_A == 0 and num_fields_B > 0:
#             #     for f_id, COMb_x, COMb_y, peak_FRb in COMb:
#             #         appeared.append([
#             #             animal, session_id, int(electrode), int(unit), 0, f_id,
#             #             0, 0, 0, 0, sicB, 0, len(COMb), 0, peak_FRb, 0, None,
#             #             None, COMb_x, COMb_y
#             #         ])
#             #
#             # # 3. fields that don't have an overlapping pair
#             # fields_num_a = len(COMShiftMatrix)
#             # for i in range(fields_num_a):
#             #     field_A_id = i + 1
#             #
#             #     if field_A_id not in A_used_fields:
#             #         peak_FRa = COMa[COMa[:, 0] == field_A_id][0][3]
#             #         COMa_x = COMa[COMa[:, 0] == field_A_id][0][1]
#             #         COMa_y = COMa[COMa[:, 0] == field_A_id][0][2]
#             #
#             #         remapped_A.append([
#             #             animal, session_id, int(electrode), int(unit), field_A_id, 0,
#             #             0, 0, 0, sicA, sicB, len(COMa), len(COMb), peak_FRa, 0, 0, COMa_x,
#             #             COMa_y, None, None
#             #         ])
#             #
#             # fields_num_b = len(COMShiftMatrix[0]) if len(COMShiftMatrix) > 0 else 0
#             # for i in range(fields_num_b):
#             #     field_B_id = i + 1
#             #
#             #     if field_B_id not in B_used_fields:
#             #         peak_FRb = COMb[COMb[:, 0] == field_B_id][0][3]
#             #         COMb_x = COMb[COMb[:, 0] == field_B_id][0][1]
#             #         COMb_y = COMb[COMb[:, 0] == field_B_id][0][2]
#             #
#             #         remapped_B.append([
#             #             animal, session_id, int(electrode), int(unit), 0, field_B_id,
#             #             0, 0, 0, sicA, sicB, len(COMa), len(COMb), 0, peak_FRb, 0, None,
#             #             None, COMb_x, COMb_y
#             #         ])
#
#     print("PF match found for %d fields, %d fields remapped" % (len(shifted), len(remapped_A) + len(remapped_B)))
#
#     if exclude_locations:
#         shifted = np.array([x[2:] for x in shifted])
#         remapped_A = np.array([x[2:] for x in remapped_A])
#         remapped_B = np.array([x[2:] for x in remapped_B])
#         appeared = np.array([x[2:] for x in appeared])
#         disappeared = np.array([x[2:] for x in disappeared])
#
#     return shifted, remapped_A, remapped_B, appeared, disappeared


def get_field_pairs(shift_matrix):
    sm = shift_matrix.copy()
    field_pairs = []

    # sort matrix by best field overlaps
    sm = sm[sm[:, 2].argsort()[::-1]]
    while len(sm) > 0 and sm[0][2] > 0:
        field_pairs.append(sm[0])  # best match on top
        f_A, f_B = sm[0][0], sm[0][1]
        sm = sm[(sm[:, 0]!=f_A) & (sm[:, 1]!=f_B)] # remove used fields

    return field_pairs


def get_fields_between_epochs(source, sessions, cond_A, cond_B):
    field_match_matrix = []
    totals = {'A': 0, 'B': 0}

    def collect_match_stats(h5_path):
        match_recs = []
        with h5py.File(h5_path, 'r') as f:
            for unit_g in f['units']:
                electrode, unit = unit_g.split('_')[0], unit_g.split('_')[1]
                unit_group = f['units']['%s_%s' % (electrode, unit)]

                shift_name = "field_shifts_%s_%s" % (cond_A, cond_B)
                if not shift_name in unit_group or not len(np.array(unit_group[shift_name])) > 0:
                    continue  # shift matrix non existing or empty

                # unit metrics
                isoldist = unit_group.attrs['isolation_distance'][0]
                sicA = unit_group[cond_A]['04_firing_rate_map'].attrs['information_content'][0]
                sicB = unit_group[cond_B]['04_firing_rate_map'].attrs['information_content'][0]
                xy_range = np.array(unit_group.attrs['xy_range'])

                # field data
                fieldsA = np.array(unit_group[cond_A][H5NAMES.field_patches])
                fieldsB = np.array(unit_group[cond_B][H5NAMES.field_patches])
                COMa = np.array(unit_group[cond_A][H5NAMES.field_clusters])
                COMb = np.array(unit_group[cond_B][H5NAMES.field_clusters])

                totals['A'] = totals['A'] + len(np.unique(fieldsA)) - 1
                totals['B'] = totals['B'] + len(np.unique(fieldsB)) - 1

                shift_matrix = np.array(unit_group[shift_name])
                field_pairs = get_field_pairs(shift_matrix)

                for pair in field_pairs:
                    f_A, f_B, overlap = pair[0], pair[1], pair[2]
                    peak_FRa = COMa[COMa[:, 0] == f_A][0][3]
                    peak_FRb = COMb[COMb[:, 0] == f_B][0][3]

                    COMa_x = COMa[COMa[:, 0] == f_A][0][1]
                    COMa_y = COMa[COMa[:, 0] == f_A][0][2]
                    COMb_x = COMb[COMb[:, 0] == f_B][0][1]
                    COMb_y = COMb[COMb[:, 0] == f_B][0][2]

                    size_A = len(fieldsA[fieldsA == f_A])
                    size_B = len(fieldsB[fieldsB == f_B])

                    match_recs.append([animal, session_id, int(electrode), \
                        int(unit), f_A, f_B, overlap, sicA, sicB, \
                        isoldist, peak_FRa, peak_FRb,\
                        COMa_x, COMa_y, COMb_x, COMb_y, COMb_y - COMa_y, \
                        size_A, size_B])
        return match_recs

    for i, (animal, session_id, s_type) in enumerate(sessions):
        sessionpath = os.path.join(source, animal, session_id)
        h5_path = os.path.join(sessionpath, 'all.h5')

        if not os.path.exists(h5_path):
            print('No required H5 file found for %s' % session_id)
            continue

        field_match_matrix += collect_match_stats(h5_path)

    # build dataframe from field_match_mat
    columns = ['animal', 'session', 'electrode', 'unit', 'field_A', 'field_B', \
        'overlap_norm', 'sic_A', 'sic_B', 'isol_dist', 'peak_FR_A', 'peak_FR_B', \
        'COMa_x', 'COMa_y', 'COMb_x', 'COMb_y', 'shift', 'size_A', 'size_B']
    df = pd.DataFrame(field_match_matrix, columns=columns)

    print("PF match found for %s fields from total %s %s and %s %s fields" % \
        (len(field_match_matrix), totals['A'], cond_A, totals['B'], cond_B))

    return df


def get_lonely_fields(source, animal, session, cond_A, cond_B):
    sessionpath = os.path.join(source, animal, session)
    h5_path = os.path.join(sessionpath, '%s.h5' % H5NAMES.filebase)

    if not os.path.exists(h5_path):
        print('No required H5 file found for %s' % session)
        return None

    lonely = []
    with h5py.File(h5_path, 'r') as f:
        for ug in f['units']:
            electrode, unit = ug.split('_')[0], ug.split('_')[1]
            unit_group = f['units']['%s_%s' % (electrode, unit)]
            isoldist = unit_group.attrs['isolation_distance'][0]

            if not 'field_shifts_%s_%s' % (cond_A, cond_B) in unit_group:
                continue

            sm = np.array(unit_group['field_shifts_%s_%s' % (cond_A, cond_B)])
            f_pairs = np.array(get_field_pairs(sm))
            try:
                if f_pairs.shape[0] > 0:
                    used_A = f_pairs[:, 0]
                else:
                    used_A = []
                if f_pairs.shape[0] > 0:
                    used_B = f_pairs[:, 1]
                else:
                    used_B = []
            except IndexError:
                print(animal, session, electrode, unit, f_pairs, sm)
                raise
            f_A = [(x, cond_A) for x in np.unique(sm[:, 0]) if not x in used_A]
            f_B = [(x, cond_B) for x in np.unique(sm[:, 1]) if not x in used_B]

            for f_id, cond in f_A + f_B:
                sic = unit_group[cond]['04_firing_rate_map'].attrs['information_content'][0]

                fields = np.array(unit_group[cond][H5NAMES.field_patches])
                size = len(fields[fields == f_id])

                COM = np.array(unit_group[cond][H5NAMES.field_clusters])
                COM_x = COM[COM[:, 0] == f_id][0][1]
                COM_y = COM[COM[:, 0] == f_id][0][2]
                peak_FR = COM[COM[:, 0] == f_id][0][3]

                lonely.append([animal, session, int(electrode), int(unit), cond, \
                               f_id, sic, isoldist, peak_FR, COM_x, COM_y, size])


    columns = ['animal', 'session', 'electrode', 'unit', 'cond', 'field_id', \
            'sic', 'isol_dist', 'peak_FR', 'COMa_x', 'COMa_y', 'size']
    return pd.DataFrame(lonely, columns=columns)


def get_field_match(h5path, electrode, unit, field_id, cond_A, cond_B):
    with h5py.File(h5path, 'r') as f:
        unit_name = '%s_%s' % (electrode, unit)
        fm_name = 'field_shifts_%s_%s' % (cond_A, cond_B)
        if not fm_name in f['units'][unit_name]:
            return None

        shift_matrix = np.array(f['units'][unit_name][fm_name])
        COMa = np.array(f['units'][unit_name][cond_A][H5NAMES.field_clusters])
        COMb = np.array(f['units'][unit_name][cond_B][H5NAMES.field_clusters])

    # filter by field_A_id and sort by overlap
    filt_idxs = shift_matrix[:, 0] == field_id
    sort_idxs = np.argsort(shift_matrix[filt_idxs][:, 2])[::-1]

    # get field with max overlap and include it if overlap > 0
    best_match = shift_matrix[filt_idxs][sort_idxs]
    if not len(best_match) > 0:
        return None

    field_match_id = best_match[0][1]
    overlap = best_match[0][2]
    COMa_y = COMa[COMa[:, 0] == field_id][0][2]
    COMb_y = COMb[COMb[:, 0] == field_match_id][0][2]

    return field_match_id, COMb_y - COMa_y
