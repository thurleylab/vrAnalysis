function COM = computeCOMs(h5path, electrode, unit, condition)

condGroup = h5info(h5path, ['/' electrode '_' unit '/' condition '/']);
XYRange = h5readatt(h5path, ['/' electrode '_' unit '/'], 'xy_range');

% allocate array assuming max 5 fields per unit
COM = zeros(length(condGroup.Datasets) * 5, 5);
fieldCount = 0;

for groupIdx=1:length(condGroup.Datasets)  % iterate over boot resamples
    W = isempty(strfind(condGroup.Datasets(groupIdx).Name, 'boot'));
    if W
        continue
    end
    
    spl = split(condGroup.Datasets(groupIdx).Name, '_');
    bootIterId = str2double(spl(2));
    
    bootPath = ['/' electrode '_' unit '/' condition '/' condGroup.Datasets(groupIdx).Name '/'];
    placeField = h5read(h5path, bootPath);
    
    [pfield, mapI] = placefield2D(placeField, 20, 0.5 * max(placeField(:)));
   
    % X and Y positions as matrixes
    [y_size, x_size] = size(placeField);  % numbers of bins in X, Y
    x_lin = linspace(XYRange(1), XYRange(2), x_size);
    x_pos = repelem(x_lin, y_size, 1);

    y_lin = linspace(XYRange(3), XYRange(4), y_size);
    y_pos = repelem(y_lin.', 1, x_size);

    if ~isfield(mapI, 'selected')
        selected = zeros(size(mapI.binary));
    else
        selected = mapI.selected;
    end

    %% compute COMs
    nFields = length(unique(selected)) - 1;  % number of place fields

    if nFields > 0
        for i=1:nFields
            fA = find(selected == i);  % linear indexes of bins inside the field
            fRate = placeField(fA);  % field firing only for this field

            COMx = sum(fRate .* x_pos(fA)) / sum(fRate);
            COMy = sum(fRate .* y_pos(fA)) / sum(fRate);

            fieldCount = fieldCount + 1;
            COM(fieldCount,:) = [bootIterId, i, COMx, COMy, max(fRate)];
        end
    end

end

COM = COM(1:fieldCount, :);

%fprintf('Pure execution took %s\n', datestr(d2 - d1,'SS.FFF'))