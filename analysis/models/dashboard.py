import os
import time
import numpy as np

from datetime import datetime


def create_data_generator(filepath):
    logfile = open(filepath, "r")

    def data_gen():
        logfile.seek(0, 2)
        while True:
            line = logfile.readline()
            if not line:
                continue
            yield line

    return data_gen


def create_frame_callable(axes):
    def run(data, xdata, ydata):
        t, x, y, z = data.split(" ")

        xdata.append(float(x))
        ydata.append(float(y))

        axes.lines[0].set_data(xdata, ydata)

    return run


def get_recent_session_name(where):
    session_names = {}

    for name in os.listdir(where):
        try:
            t1 = datetime.strptime(name, '%Y-%m-%d_%H-%M-%S')
            session_names[int(time.mktime(t1.timetuple()))] = name
        except ValueError:
            pass  # ignore non-session folders / files

    return session_names[int(np.array(list(session_names.keys())).max())]
