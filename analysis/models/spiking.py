import numpy as np

from analysis.models.vr import RatCAVESession


def unit_data(sessionpath, ttd_idx, unit_idx):
    session = RatCAVESession(sessionpath)
    running_idxs = session.get_running_indexes()[0]

    phi = np.pi * 4.3 / 180.0
    rot_m = np.array([[np.cos(phi), -np.sin(phi)], [np.sin(phi), np.cos(phi)]])

    # all positions with time, rotated: t, x, y
    all_pos = np.array([np.concatenate((x[0], np.dot(rot_m, x[1:3])), axis=None) for x in session.trajectory])

    # unit positions with time, rotated: t, x, y
    unit_pos = session.get_positions_for_unit(ttd_idx, unit_idx, running_idxs)
    unit_pos = np.array([np.concatenate((x[0], np.dot(rot_m, x[1:3])), axis=None) for x in unit_pos])

    return all_pos, unit_pos
