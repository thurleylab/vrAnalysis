import numpy as np
from scipy import signal


def gaussian_kernel_2D(sigma=0.1):
    lin_profile = np.linspace(-10, 10, 30)
    bump = np.exp(-sigma * lin_profile**2)
    bump /= np.trapz(bump)  # normalize to 1
    return bump[:, np.newaxis] * bump[np.newaxis, :]


def place_field_2D(pos, pos_firing, sampling_rate, bin_size=0.05, sigma=0.1, xy_range=None):
    """
    :param pos:             x, y positions sampled at sampling_rate
    :param pos_firing:      positions when spikes occured
    :param sampling_rate:   sampling rate of above
    :param bin_size:        size of the squared bin to calculate firing / occupancy,
                                same units as in pos, e.g. meters
    :param sigma:           standard deviation, for smoothing
    :param range:           [xmin, xmax, ymin, ymax] - array of X and Y boundaries to limit the map
    :return:
                            occupancy_map, spiking_map, firing_map, s_firing_map
    """
    x_min = xy_range[0] if xy_range is not None else pos[:, 0].min()
    x_max = xy_range[1] if xy_range is not None else pos[:, 0].max()
    y_min = xy_range[2] if xy_range is not None else pos[:, 1].min()
    y_max = xy_range[3] if xy_range is not None else pos[:, 1].max()

    x_range = x_max - x_min
    y_range = y_max - y_min

    y_bin_count = int(np.ceil(y_range / bin_size))
    x_bin_count = int(np.ceil(x_range / bin_size))

    pos_range = np.array([[x_min, x_max], [y_min, y_max]])

    occup, x_edges, y_edges = np.histogram2d(pos[:, 0], pos[:, 1], bins=[x_bin_count, y_bin_count], range=pos_range)
    occupancy_map = occup / sampling_rate

    # spiking map
    spiking_map, xs_edges, ys_edges = np.histogram2d(pos_firing[:, 0], pos_firing[:, 1], bins=[x_bin_count, y_bin_count], range=pos_range)

    # firing = spiking / occupancy
    firing_map = np.divide(spiking_map, occupancy_map, out=np.zeros_like(spiking_map, dtype=float), where=occupancy_map!=0)

    # apply gaussial smoothing
    kernel = gaussian_kernel_2D(sigma)
    s_firing_map = signal.convolve2d(firing_map, kernel, mode='same')

    return occupancy_map, spiking_map, firing_map, s_firing_map


def direction_preference(phases, firing, bins=100):
    """
    Compute direction preference for a given neuronal firing.

    :param phases:      1D array of angles in degrees (length N)
    :param firing:      1D array of mean firing rates (length N)
    :param bins:        int, how to bin the circular space
    :return:            1D array of preferences, 1D array of binned angles
    """
    space = np.linspace(-180, 180, bins)

    preference = []

    for i, b0 in enumerate(space[:-1]):
        b1 = space[i + 1]

        preference.append(firing[(phases > b0) & (phases < b1)].mean())

    return preference, space
