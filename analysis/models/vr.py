import os
import numpy as np
import json

from scipy import signal
from analysis.adapters.vr import load_hotspots, load_positions, load_autocorrelogram, load_unit_positions
from analysis.adapters.kwik import guess_filebase, guess_indexes, load_clu_res_single
from analysis.adapters.openephys import load_events


class VRSession(object):

    filenames = {
        "position": "positions.traj",
        "collision": "collisions.traj",
        "hotspot": "hotspots.traj",
        "external": "external.traj",
    }

    def __init__(self, path):
        # 7 x N matrix: absolute time in s, X, Y, Z, pitch, yaw, roll
        self.trajectory = load_positions(os.path.join(path, VRSession.filenames['position']))
        if len(self.trajectory) < 10:
            raise ValueError("%s session has empty trajectory" % path)

        # exclude duplicate rows
        dt = np.diff(self.trajectory, axis=0)[:, 0]
        self.trajectory = np.delete(self.trajectory, np.where(dt == 0)[0], 0)

        # 4 x N matrix: absolute time in s, X, Y, Z
        self.collisions = load_positions(os.path.join(path, VRSession.filenames['collision']))

        # 7 x N matrix: absolute time in s, spot ID, X, Y, Z, x_hotspot, y_hotspot
        self.hotspots = load_hotspots(os.path.join(path, VRSession.filenames['hotspot']))

        try:
            jsonfile = [x for x in os.listdir(path) if '.json' in x][0]

            with open(os.path.join(path, jsonfile)) as f:
                self.settings = json.load(f)
        except IndexError:
            self.settings = {}

    def position_index_at(self, t0):
        return np.abs(self.trajectory[:, 0] - t0).argmin()

    def position_at(self, t0):
        return self.trajectory[self.position_index_at(t0)]

    def distance(self, t0, t1):
        t0_idx = np.abs(self.trajectory[:, 0] - t0).argmin()
        t1_idx = np.abs(self.trajectory[:, 0] - t1).argmin()

        diffs = np.diff(self.trajectory[t0_idx : t1_idx], axis=0)
        dx = np.sqrt(np.square(diffs[:, 1]) + np.square(diffs[:, 2]))

        return dx.sum()

    def distance_smoothed(self, ker_width=200):
        diffs = np.diff(self.trajectory, axis=0)
        dx = np.sqrt(np.square(diffs[:, 1]) + np.square(diffs[:, 2]))

        # gaussian kernel
        kernel = signal.gaussian(ker_width + 1, std=(ker_width + 1) / 7.2)

        # linear kernel
        #kernel = np.ones(ker_width)

        cvd = np.convolve(dx, kernel, 'same') / kernel.sum()
        return np.column_stack((self.trajectory[:-1, 0], cvd))

    @property
    def t_start(self):  # in s since some point in time - timestamp
        return self.trajectory[0][0]

    @property
    def t_end(self):  # in s
        return self.trajectory[-1][0]

    @property
    def duration(self):  # in s
        return self.t_end - self.t_start

    @property
    def velocity(self):
        """
        :return: Nx2 Matrix, d1 - time, d2 - speed
        """
        diffs = np.diff(self.trajectory, axis=0)

        dt = diffs[:, 0]
        dx = np.sqrt(np.square(diffs[:, 1]) + np.square(diffs[:, 2]))

        return np.column_stack((self.trajectory[:-1, 0], dx/dt))

    def smoothed_velocity(self, ker_width=100):  # 2D array, uses self.velocity[:, 0] as time at 0th column
        # gaussian kernel
        kernel = signal.gaussian(ker_width + 1, std=(ker_width + 1) / 7.2)
        velocity = self.velocity

        cvd = np.convolve(velocity[:, 1], kernel, 'same') / kernel.sum()
        return np.column_stack((self.trajectory[:-1, 0], cvd))

    def head_direction(self, ker_width=100):
        """
        In the horizontal plane.

        :param ker_width:   width of the gaussial kernel for smoothing.
        :return:            1D smoothed angular array in degrees.
        """
        kernel = signal.gaussian(ker_width + 1, std=(ker_width + 1) / 7.2)

        angles = np.unwrap(self.trajectory[:, 5] * (np.pi / 180.0))  # unwrap for correct convolution
        smoothed = np.convolve(angles, kernel, 'same') / kernel.sum()

        return (np.mod(smoothed, 2 * np.pi) - np.pi) * (180. / np.pi)

    @property
    def running_time(self, threshold=0.15):
        run_idxs = np.where(self.smoothed_velocity[:, 1] > threshold)[0]  # 0.15 m/s
        time_diffs = np.diff(self.trajectory[run_idxs][:, 0])
        return time_diffs[np.where(time_diffs < 0.2)[0]].sum()

    @property
    def collision_time(self):
        if len(self.collisions) > 0:
            coll_diffs = np.diff(self.collisions[:, 0])
            return coll_diffs[np.where(coll_diffs < 0.1)[0]].sum()
        return 0

    @property
    def IRI(self):
        return np.diff(self.hotspots[:, 0]) if len(self.hotspots) > 1 else []

    @property
    def IRD(self):
        if len(self.hotspots) < 2:
            return []

        distances = []

        for i, spottime in enumerate(self.hotspots[:-1, 0]):
            distances.append(self.distance(self.hotspots[i][0], self.hotspots[i + 1][0]))

        return np.array(distances)

    def get_running_indexes(self, threshold=0.15):  # 0.15 m/s
        return np.where(self.smoothed_velocity()[:, 1] > threshold)[0]

    def get_positions_at_freq(self, freq):
        t_start = self.trajectory[0][0]
        result = []

        samples_to_create = int(round((self.trajectory[-1][0] - self.trajectory[0][0]) * freq))
        for ti in [t_start + x / float(freq) for x in range(samples_to_create)]:
            nearest_row = self.trajectory[np.abs(self.trajectory[:, 0] - ti).argmin()]
            result.append((nearest_row[1], nearest_row[2]))

        return np.array(result)


class EphysSession(object):

    filenames = {
        "events": "messages.events"
    }

    def __init__(self, path):
        filebase = guess_filebase(path)
        indexes = guess_indexes(path)

        self.tetrodes = dict([(i, load_clu_res_single(path, filebase, i)) for i in indexes])

        self.frequency, self.events = load_events(os.path.join(path, EphysSession.filenames['events']))

    @property
    def tetrode_unit_map(self):
        """ A dict of "tetrode number": [units] """
        return dict([(ttd, [k for k in units.keys()]) for ttd, units in self.tetrodes.items()])

    @property
    def units(self):
        """ Each unit is just a 1D array (spiketrain) of samples at self.frequency, like
                array([93772, 138840, 165903, ...])
        """
        return [unit for ttd in self.tetrodes.values() for unit in ttd.values()]


class RecordingSession(VRSession, EphysSession):

    filenames = {
        "autocorr": "units",
        "position": "units"
    }

    def __init__(self, path):
        VRSession.__init__(self, path)
        EphysSession.__init__(self, path)

        self.path = path

        sample_map = []  # position in sec, events in samples (like 30kHz)

        for i, traj_event in enumerate(self.trajectory[:, 0]):
            nearest_event_idx = np.abs(self.events[:, 1] - traj_event).argmin()
            time_from_nearest = traj_event - self.events[nearest_event_idx][1]

            sample_map.append([self.events[nearest_event_idx][0]] + time_from_nearest * self.frequency)

        self.trajectory = np.concatenate((self.trajectory, np.array(sample_map)), axis=1)

        # loading autocorrelograms / unit positions - if any
        self._autocorrs = {}
        self._unit_poss = {}
        aurocorrs_path = os.path.join(self.path, RecordingSession.filenames["autocorr"])
        unit_poss_path = os.path.join(self.path, RecordingSession.filenames["position"])

        for ttd, units in self.tetrode_unit_map.items():
            autocorrs = {}
            positions = {}
            for unit in units:
                a_path = os.path.join(aurocorrs_path, 'ttd_%s_unit_%s_autocorr.txt' % (ttd, unit))
                p_path = os.path.join(unit_poss_path, 'ttd_%s_unit_%s_position.txt' % (ttd, unit))

                autocorrs[unit] = load_autocorrelogram(a_path) if os.path.exists(a_path) else None
                positions[unit] = load_unit_positions(p_path) if os.path.exists(p_path) else None

            self._autocorrs[ttd] = autocorrs
            self._unit_poss[ttd] = positions

    def get_sync_data(self):
        return np.column_stack((self.trajectory[:, 0], self.trajectory[:, -1]))

    def get_unit_firing_at_freq(self, ttd_idx, unit_idx, freq):
        return np.round((self.tetrodes[ttd_idx][unit_idx] / float(self.frequency)) * freq).astype(np.uint32)

    def get_spike_times(self, ttd, unit):  # in seconds, trajectory times
        spike_times = []

        for spk_idx in self.tetrodes[ttd][unit]:
            try:
                spike_times.append(self.get_time_by_spike_index(spk_idx))  # spk_idx in ephys samples
            except IndexError:
                pass  # outside of recording intervals
        return np.array(spike_times)

    def get_spike_indexes(self, ttd, unit):  # trajectory indexes (convert samples to trajectory indexes)
        stimes = self.get_spike_times(ttd, unit)

        return np.array([np.abs(self.trajectory[:, 0] - x).argmin() for x in stimes])

    def get_spiketrain(self, ttd, unit):
        """
        Get 0/1 spiketrain for a given ttd / unit as a 0-1 array of the trajectory length.

        :param ttd:     tetrode index (1-indexed)
        :param unit:    unit index  (1-indexed)
        :return:        1D array of zeros and ones
        """
        spiketrain = np.zeros(len(self.trajectory))

        stimes = self.get_spike_times(ttd, unit)
        idxs = np.array([np.abs(self.trajectory[:, 0] - x).argmin() for x in stimes])

        spiketrain[idxs] = 1

        return spiketrain

    def get_positions_for_unit(self, ttd, unit, allowed_idxs=(), refresh=False):
        if not refresh and self._unit_poss[ttd][unit] is not None:
            return self._unit_poss[ttd][unit]

        positions = []
        include_all = bool(len(allowed_idxs) == 0)

        spike_times = self.get_spike_times(ttd, unit)
        for spike_time in spike_times:
            try:
                pos_idx = self.position_index_at(spike_time)
            except IndexError:
                pass  # outside of recording intervals

            if include_all or pos_idx in allowed_idxs:
                positions.append(self.trajectory[pos_idx])

        self._unit_poss[ttd][unit] = np.array(positions)[:, :4]

        # save to disk
        units_path = os.path.join(self.path, RecordingSession.filenames["position"])
        np.savetxt(os.path.join(units_path, 'ttd_%s_unit_%s_position.txt' % (ttd, unit)), self._unit_poss[ttd][unit])

        return self._unit_poss[ttd][unit]

    def get_positions_for_unit2(self, ttd, unit, allowed_idxs=()):
        pos = []
        include_all = bool(len(allowed_idxs) == 0)

        for sample in self.tetrodes[ttd][unit]:
            idx = np.abs(self.trajectory[:, -1] - sample).argmin()
            if include_all or idx in allowed_idxs:
                pos.append(self.trajectory[idx])

        return np.array(pos)

    def get_unit_positions_with_autocorrs(self, allowed_idxs=()):
        unit_pos_dict = {}

        for ttd, units in self.tetrode_unit_map.items():
            data = {}
            for unit in units:
                print("\rTTD %s Unit %s data is being fetched.." % (ttd, unit))  # , end=""

                position = self.get_positions_for_unit(ttd, unit, allowed_idxs)
                autocorr = self.get_autocorr(ttd, unit)
                data[unit] = (position, autocorr)

            unit_pos_dict[ttd] = data

        print("\rCompleted.")
        return unit_pos_dict

    def get_time_by_spike_index(self, sample_index):
        t0_idx = np.abs(self.trajectory[:, -1] - sample_index).argmin()

        shift = (sample_index - self.trajectory[t0_idx][-1]) / self.frequency  # in sec

        return self.trajectory[t0_idx][0] + shift

    def get_spike_index_at_time(self, t0):  # in s
        t0_idx = np.abs(self.trajectory[:, 0] - t0).argmin()

        shift = (t0 - (self.trajectory[t0_idx][0] - self.trajectory[0][0])) * self.frequency  # in samples

        return int(np.round(self.trajectory[t0_idx][-1] + shift))

    # analytics

    def get_autocorr(self, ttd, unit, window=60, refresh=False):
        """
        histogram of inter-spike time differences

        :param ttd:     group id
        :param unit:    unit id
        :param window:  in milliseconds
        :return:
        """
        if not refresh and self._autocorrs[ttd][unit] is not None:
            return self._autocorrs[ttd][unit]

        spike_times = self.get_spike_times(ttd, unit)
        hist = np.zeros(window)
        bins = np.arange(window + 1) - window / 2

        if len(spike_times) < window:
            return hist

        for i, spike_time in enumerate(spike_times):
            diffs = (spike_times - spike_time) * 10 ** 3  # sec to millisecs
            diffs_w = [x for x in diffs if abs(x) < window / 2 and not abs(x) < 10 ** -6]

            hist += np.histogram(diffs_w, bins=bins)[0]

        self._autocorrs[ttd][unit] = hist

        # save to disk
        units_path = os.path.join(self.path, RecordingSession.filenames["autocorr"])
        np.savetxt(os.path.join(units_path, 'ttd_%s_unit_%s_autocorr.txt' % (ttd, unit)), hist)

        return hist

    def get_firing_smoothed(self, ttd, unit, ker_width=500):  # 5 secs assuming samping at 100Hz
        window = signal.gaussian(ker_width, std=(ker_width + 1) / 7.2)

        return np.convolve(self.get_spiketrain(ttd, unit), window, 'same')


class RatCAVESession(RecordingSession):

    def __init__(self, path, alpha=0):
        """
        :param path:    path to the folder with Session files
        :param alpha:   angle to rotate coordinates in the horizontal plane in deg.
        :param center:  place the coordinate system into arena center (x, y) only
        """
        def get_minmax(data, delta_min, delta_max):
            vals, periods = np.histogram(data, 50)  # position distribution
            selected= []
            check_max = True

            for i, curr_per in enumerate(periods[:-1]):
                if check_max and vals[i] > 100 or not check_max and vals[i] < 100:
                    selected.append(curr_per)
                    check_max = not check_max

            if len(selected) % 2 > 0:
                selected.append(periods[-1])

            for i in range(int(len(selected)/2)):
                delta = selected[i*2 + 1] - selected[i*2]
                if delta_min < delta < delta_max:
                    return selected[i*2], selected[i*2 + 1]

            raise ValueError("Positions outside arena could not be detected.")

        RecordingSession.__init__(self, path)

        phi = np.pi * alpha / 180.0
        rot_m = np.array([[np.cos(phi), -np.sin(phi)], [np.sin(phi), np.cos(phi)]])

        # swap Y - Z for ratCAVE recordings
        self.trajectory[:, [2, 3]] = self.trajectory[:, [3, 2]]

        # rotate in the horizontal plane to match OptiTrack / Virtual coord. systems
        pos_xy_rot = np.array([np.dot(rot_m, x[1:3]) for x in self.trajectory])
        self.trajectory[:, [1, 2]] = pos_xy_rot

        # load arena trajectory
        self.arena_trajectory = []
        if os.path.exists(os.path.join(path, 'arena.traj')):
            self.arena_trajectory = load_positions(os.path.join(path, 'arena.traj'))

        if len(self.arena_trajectory) > 0:
            # swap Y - Z for ratCAVE recordings
            self.arena_trajectory[:, [2, 3]] = self.arena_trajectory[:, [3, 2]]

            # rotate in the horizontal plane to match OptiTrack / Virtual coord. systems
            pos_xy_rot = np.array([np.dot(rot_m, x[1:3]) for x in self.arena_trajectory])
            self.arena_trajectory[:, [1, 2]] = pos_xy_rot

        # patch positions outside arena
        running_idxs = self.get_running_indexes()

        delta = 0.05  # 10 cm larger than arena

        x_min, x_max = get_minmax(self.trajectory[:, 1], 0.6, 0.9)
        y_min, y_max = get_minmax(self.trajectory[:, 2], 1.4, 2.0)

        idxs_inside_x = np.where((self.trajectory[:, 1] < x_max + delta) & (self.trajectory[:, 1] > x_min - delta))[0]
        idxs_inside_y = np.where((self.trajectory[:, 2] < y_max + delta) & (self.trajectory[:, 2] > y_min - delta))[0]
        idxs_inside = np.intersect1d(idxs_inside_x, idxs_inside_y)
        idxs_outside = np.setdiff1d(np.arange(len(self.trajectory)), idxs_inside)

        delta_x = x_max - x_min
        delta_y = y_max - y_min

        x_center = x_min + (x_max - x_min) / 2
        y_center = y_min + (y_max - y_min) / 2

        # center to center of the coordinate system
        self.trajectory[:, 1] -= x_center
        self.trajectory[:, 2] -= y_center

        if len(idxs_outside) > 0:
            self.trajectory[idxs_outside, 1] = 0  # set X to center
            self.trajectory[idxs_outside, 2] = 0  # set Y to center
            #new_idxs = np.intersect1d(idxs_inside, running_idxs)
