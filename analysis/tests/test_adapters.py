import unittest
import os
import numpy as np

from ..adapters.vr import load_hotspots, load_positions, get_lines_to_skip


class TestVRSession(unittest.TestCase):
    """
    Run tests from main project folder like 'python analysis/tests/run.py'
    """

    def setUp(self):
        self.where = os.path.join(os.path.dirname(__file__), "resources")

    def tearDown(self):
        pass

    def test_positions(self):
        path = os.path.join(self.where, "positions.traj")

        trajectory = load_positions(path)
        with open(path) as f:
            num_lines = sum(1 for line in f)

        assert(len(trajectory) == num_lines - get_lines_to_skip(path))
        assert(trajectory.dtype == np.float64)

    def test_hotspots(self):
        path = os.path.join(self.where, "hotspots.traj")

        spots = load_hotspots(path)
        with open(path) as f:
            num_lines = sum(1 for line in f)

        assert(len(spots) == num_lines - get_lines_to_skip(path))
        assert(spots.dtype == np.float64)
