import unittest
import os
import numpy as np

from ..models.vr import VRSession
from ..models.vr import EphysSession
from ..models.vr import RecordingSession


class TestVRSession(unittest.TestCase):
    """
    Run tests from main project folder like 'python analysis/tests/run.py'
    """

    def setUp(self):
        self.session = VRSession(os.path.join(os.path.dirname(__file__), "resources"))

    def tearDown(self):
        pass

    def test_velocity(self):
        velocity = self.session.velocity

        delta = self.session.trajectory[1] - self.session.trajectory[0]
        dt = delta[0]
        dx = np.sqrt(delta[1]**2 + delta[2]**2)

        assert(self.session.velocity[0][1] == dx/dt)


class TestEphysSession(unittest.TestCase):
    """
    Run tests from main project folder like 'python analysis/tests/run.py'
    """

    def setUp(self):
        self.where = os.path.join(os.path.dirname(__file__), "resources")
        self.session = EphysSession(self.where)

    def tearDown(self):
        pass

    def test_init(self):
        clu_files = [x for x in os.listdir(self.where) if x.find('.clu.') > -1]

        assert(len(self.session.tetrodes) == len(clu_files))


class TestRecordingSession(unittest.TestCase):
    """
    Run tests from main project folder like 'python analysis/tests/run.py'
    """

    def setUp(self):
        self.where = os.path.join(os.path.dirname(__file__), "resources")
        self.session = RecordingSession(self.where)

    def tearDown(self):
        pass

    def test_init(self):
        spki = self.session.get_spike_index_at_time(0)

        positions = []

        u1 = self.session.units[1]  # 1D array of spike times as samples
        for spk_idx in u1:
            try:
                t_spike = self.session.get_time_by_spike_index(spk_idx)
                positions.append(self.session.position_at(t_spike))
            except IndexError:
                pass  # outside of recording intervals

        u1_positions = np.array(positions)