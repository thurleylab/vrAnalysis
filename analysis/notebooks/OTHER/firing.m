% computing FIRING RATE maps

sessionpath = '/home/andrey/storage2/andrey/data/processed/002430/2019-03-04_11-58-58';
analysis_dir = 'analysis';
units_dir = 'units';

% animal position within the session
pos = load(fullfile(sessionpath, 'pos_at_50Hz.pos'));

% get number of unit files
unit_files = dir(fullfile(sessionpath, units_dir, '*.spt'));

figure('Name','2D Firing rate map')
ax_x_axes = 2;
ax_y_axes = ceil((length(unit_files) / ax_x_axes));

i = 1;
for file = unit_files'
    % loading spiketrain data
    spiket = load(fullfile(sessionpath, units_dir, file.name));

    % scale and shift al the values to work with PlaceField2D script
    scaling = 1;
    shift = 1.5;
    %scaling = 4e4;
    %shift = 2e5;
    pos_scaled = pos * scaling + shift;

    % compute rate maps
    [sRateMap, OccupMap, SpkMap, Bin1_scaled, Bin2_scaled] = PlaceField2D(spiket, pos_scaled, [], 0.03, [3 3], [], 0);

    % scale and shift back to original position
    Bin1 = (Bin1_scaled - shift) / scaling;
    Bin2 = (Bin2_scaled - shift) / scaling;

    %for nicer visualisation: replace firing rates in unvisited bins with 
    %the decreased value to make them black on a figure    
    sRateMap2 = sRateMap;
    sRateMap2(OccupMap==0) = min(sRateMap2(:)) - [max(sRateMap2(:)) - min(sRateMap2(:))]/63;

    %for nicer visualisation: x,y limits
    lim1 = Bin1([min(find(sum(OccupMap')>0))  max(find(sum(OccupMap')>0))]);
    lim2 = Bin2([min(find(sum(OccupMap)>0))  max(find(sum(OccupMap)>0))]);

    %smoothed rate map
    subplot(ax_y_axes, ax_x_axes, i); cla; 
    imagesc(Bin1, Bin2, sRateMap2'); 
    colormap('default'); cc = colormap; cc(1,:) = [0 0 0]; colormap(cc)
    axis xy; axis tight ; axis square 
    set(gca, 'xlim', lim1, 'ylim', lim2)
    
    m=round(max(sRateMap(:)));
    n_list = strsplit(file.name, '_');
    
    title(['G ' n_list{1, 2} ', Unit ' n_list{1, 4} ' max=' num2str(m) ' Hz'],'fontsize', 10);
    colorbar;
    
    i = i + 1;
end


% saving as image
AxisPropForIllustrator; 
SaveForIllustrator(fullfile(sessionpath, analysis_dir, 'firingrates'),'jpg');