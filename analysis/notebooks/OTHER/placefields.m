% computing FIRING RATE maps

sessionpath = '/home/andrey/storage2/andrey/data/processed/002430/2019-02-27_13-21-54';
analysis_dir = 'analysis';
units_dir = 'units';

ttd = 6;
unit = 3;

% animal position within the session
pos = load(fullfile(sessionpath, 'pos_at_50Hz.pos'));

% rotate in the horizontal plane to match OptiTrack / Virtual coord. systems
phi = pi * -4.3 / 180.0;
rot_m = [cos(phi) -sin(phi); sin(phi) cos(phi)];

pos_rot = pos * rot_m;

% scale and shift al the values to work with PlaceField2D script
scaling = 1;
shift = 1.5;
pos_scaled = pos_rot * scaling + shift;

% loading spiketrain data
filename = ['ttd_' num2str(ttd) '_unit_' num2str(unit) '_firing.spt'];
spiket = load(fullfile(sessionpath, units_dir, filename));

% figure init
figure('Name','2D Firing rate map')
ax_x_axes = 2;
ax_y_axes = 1;

lg = size(pos_scaled);
t_idx = 500;

idxs{1} = 1:t_idx;
idxs{2} = t_idx:lg(1);

spts{1} = spiket(spiket < t_idx);
spts{2} = spiket(spiket > t_idx) - t_idx;

for k=1:length(idxs)
    % compute rate maps
    [sRateMap, OccupMap, SpkMap, Bin1_scaled, Bin2_scaled] = PlaceField2D(spts{k}, pos_scaled(idxs{k},:), [], 0.03, [3 3], [], 0);

    % scale and shift back to original position
    Bin1 = (Bin1_scaled - shift) / scaling;
    Bin2 = (Bin2_scaled - shift) / scaling;

    % for nicer visualisation: replace firing rates in unvisited bins with 
    % the decreased value to make them black on a figure    
    sRateMap2 = sRateMap;
    sRateMap2(OccupMap==0) = min(sRateMap2(:)) - [max(sRateMap2(:)) - min(sRateMap2(:))]/63;

    % for nicer visualisation: x,y limits
    lim1 = Bin1([min(find(sum(OccupMap')>0))  max(find(sum(OccupMap')>0))]);
    lim2 = Bin2([min(find(sum(OccupMap)>0))  max(find(sum(OccupMap)>0))]);

    % smoothed rate map
    subplot(ax_y_axes, ax_x_axes, k); cla; 
    imagesc(Bin1, Bin2, sRateMap2'); 
    colormap('default'); cc = colormap; cc(1,:) = [0 0 0]; colormap(cc)
    axis xy; axis tight ;
    set(gca, 'xlim', lim1, 'ylim', lim2)
    
    m=round(max(sRateMap(:)));
    
    title(['Group ' num2str(ttd) ', Unit ' num2str(unit) ' max=' num2str(m) ' Hz'],'fontsize', 10);
    colorbar;
end


% saving as image
AxisPropForIllustrator;
filename = ['ttd_' num2str(ttd) '_unit_' num2str(unit) '_180_fields'];
SaveForIllustrator(fullfile(sessionpath, analysis_dir, filename),'jpg');