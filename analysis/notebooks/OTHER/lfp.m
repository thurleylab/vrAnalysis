
sessionpath = '/home/andrey/STORAGE2/andrey/data/processed/001458/2018-10-19_16-43-57';
cd '/home/andrey/STORAGE2/andrey/data/processed/001458/2018-10-19_16-43-57';

pos = load('positions.short.traj');  % positions file without headers
sync = load(fullfile('analysis', 'sync.txt'));
xml = LoadXml('all.xml');
[Res, Clu, Map ] = LoadCluRes('all');
Lfp = LoadBinary('all.lfp', 13, xml.nChannels)';

wLfp = WhitenSignal(Lfp);

%% time-frequency spectrum / coherence
% spectrum
[y,f,t] = mtchglong(wLfp, 2^12, xml.lfpSampleRate, 2^11, [], 2.5, 'linear',[], [1 100]);

% speed
dt = diff(pos(:,1));
dx = sqrt(diff(pos(:,2)).^2 + diff(pos(:,2)).^2);
speed = dx ./ dt;
speed_s = smooth(speed, 50);
t_spd = pos(1:end-1, 1) - pos(1, 1);

% plotting
figure;
title('time-frequency spectrum', 'fontsize', 12);

cla;  hold on
imagesc(t,f,log10(y)');
axis xy; colormap('default'); colorbar
ylabel('Freq, Hz'); xlabel('Time, s');
axis tight
set(gca, 'ylim', [1.5 20])

spd = speed_s;
spd = spd/max(spd)*15;
plot(t_spd, spd, 'k');



%% tetrode analysis
Lfps = LoadBinary('all.lfp', [1:4:32], xml.nChannels');
wLfps = WhitenSignal(Lfps);

[y,f,phi] = mtchd(wLfps, 2^12, xml.lfpSampleRate, 2^11, [], 1.5, 'linear',[], [1 80]);

PowMat = MatDiag(y);

figure
clf
subplot(221)
imagesc(f,[],log10(PowMat)');
ylabel('Tetrode #'); xlabel('Frequency');
title('spectral power');
colorbar

RefCh = setdiff([1:8],4);

subplot(222)
imagesc(f,[],sq(y(:,4,RefCh))');
ylabel('Tetrode #'); xlabel('Frequency');
title('coherence to ch 4');
set(gca,'YTickLabel',num2str(RefCh'));
colorbar

subplot(223)
imagesc(f,[],sq(phi(:,4,RefCh))'*180/pi);
ylabel('Tetrode #'); xlabel('Frequency');
title('phase shift to ch 4');
caxis([-pi pi]);
set(gca,'YTickLabel',num2str(RefCh'));
colorbar
CircColormap

ForAllSubplots('xlim([1 15])')


%% speed

%[SyncPos, SyncPosi, SyncDati] = NearestNeighbour(smpl, sync(:,1),'both');

Speed= sum(diff(smooth(pos(:,3:4),30)));

fLfp = ButFilter(Lfp,2,[6 12]/625,'bandpass');
hilb= hilbert(fLfp);
phase = angle(hilb);
amp = abs(hilb);

ResLfp = round(Res*1250/30000);

[ccg, tbin, pairs] = CCG(Res,Clu,5,100,30000,Map(:,1),'count');
%BarMatrix(tbin,ccg);

for k=1:size(Map,1)
    subplotfit(k, size(Map,1));
    histcirc(phase(ResLfp(Clu==k)),36);
end

% compute spectrogram in fast freq. range
[y,f,t] = mtchglong(wLfps(:,7), 2^8, xml.lfpSampleRate, 2^7, [], 2.5, 'linear',[], [50 300]);
figure;
imagesc(t,f,log10(y)'); axis xy; colormap('jet'); colorbar

% avergage spectrum in fast freq. range
[y,f,t] = mtchd(wLfps, 2^8, xml.lfpSampleRate, 2^7, [], 1, 'linear',[], [50 300]);
PowMat = MatDiag(y);
figure
imagesc(f,[],log10(PowMat)');
ylabel('Tetrode #'); xlabel('Frequency');
title('spectral power');
colorbar

%load all channels for short chunk
Lfps = bload('all.lfp',[xml.nChannels 1250*60])';
wLfps = WhitenSignal(Lfps);

%load raw data 
Dat = LoadBinary('all.dat',13,xml.nChannels)';
wDat = WhitenSignal(Dat);
figure
mtchd(wDat, 2^12, xml.SampleRate, 2^11, [], 1, 'linear',[], [30 15000]);

% power/coherence in fast time scale (see EMG and line noise harmonics)
mtchd(wLfps(:,[2 4]), 2^8, xml.lfpSampleRate, 2^7, [], 1, 'linear',[], [50 600]);
 
%detect ripples on CA1 ch 
Rips=  DetectOscillations(diff(Lfps(:,[7 4]),1,2), [120 180], 5, 1250, 3, 200);

Segs = GetSegs( Lfps, Rips.t-125,251,1);
mSegs = sq(mean(Segs,2));

Segs = GetSegs( Lfps, Rips.t_at_trough-125,251,1);
mSegs = sq(mean(Segs,2));

% TODO add labels / units
cm = colormap
c = cm(1:8:64,:);
figure
scalor = 6/2^15;
for sh=1:8
plot([-50:49]/1.25+(sh-1)*60, mSegs(126+[-50:49],[1:8]+8*(sh-1))*scalor);
%plot([-125:125]/1.25, mSegs(:,[1:8]+8*(sh-1))-(sh-1)*1000,'Color',c(sh,:));

%imagesc([-125:125]/1.25+(sh-1)*250, mSegs(:,[1:8]+8*(sh-1)));
hold on
end

rmSegs = reshape(permute(reshape(mSegs,size(mSegs,1),8,8),[1 3 2]),size(mSegs,1)*8,8);
figure
imagesc(rmSegs');




% TODO add labels / units

[TrigSpec, f,t] = TrigSpecgram(Rips.t,625, wLfps(:,4), 2^8, xml.lfpSampleRate, 2^7, [], 1, 'linear',[], [50 300]);
figure;
imagesc(t,f,log10(TrigSpec)');
xlabel('time lag from ripple'); ylabel('frequency');


