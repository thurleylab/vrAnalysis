import numpy as np


def load_positions(filepath):
    if get_line_number(filepath) == get_lines_to_skip(filepath):
        return np.array([])
    return np.loadtxt(filepath, dtype=np.float64, skiprows=get_lines_to_skip(filepath))


def load_hotspots(filepath):
    if get_line_number(filepath) == get_lines_to_skip(filepath):
        return np.array([])

    # FIXME parse different formats - make real adapters and pass them to constructor

    if 1:
        return np.array([])
    return np.loadtxt(filepath, dtype=np.float64, skiprows=get_lines_to_skip(filepath))


def load_autocorrelogram(filepath):
    return np.loadtxt(filepath, dtype=np.float64)


def load_unit_positions(filepath):
    return np.loadtxt(filepath, dtype=np.float64)


def get_lines_to_skip(filepath):
    counter = 0

    with open(filepath, 'r') as f:
        while True:
            if f.readline().startswith('#'):
                counter += 1
            else:
                break

    return counter


def get_line_number(filepath):
    with open(filepath, 'r') as f:
        counter = len(f.readlines())

    return counter
