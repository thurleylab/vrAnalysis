import h5py
import numpy as np

class H5NAMES:
    filebase = 'all'
    occupancy = '01_occupancy'
    spiking = '02_spiking'
    firing_bins = '03_firing_bins'
    firing_rate_map = '04_firing_rate_map'
    field_centers = '05_field_centers_bootstrapped'
    field_clusters = '06_field_clusters'
    field_patches = '07_field_patches'
    tpp = '08_theta_pp'
    field_shifts = 'field_shifts'
    hd = 'head_direction'
    hd_angle_delta_mean = 'angle_delta_mean'
    hd_angle_delta_weight_avg = 'angle_delta_weight_avg'
    hd_angle_delta_SD = 'angle_delta_SD'
    hd_angle_delta_n_samples = 'angle_delta_n_samples'


def store_to_h5(filepath, session):
    f = h5py.File(filepath, "w")

    # trajectory - mandatory
    f.create_dataset('animal_trajectory', data=session.trajectory)

    # arena trajectory
    f.create_dataset('arena_trajectory', data=session.arena_trajectory)

    # hotspots
    if hasattr(session, 'hotspots') and len(session.hotspots) > 0:
        f.create_dataset('hotspots', data=session.hotspots)

    # rewards
    if hasattr(session, 'rewards') and len(session.rewards) > 0:
        f.create_dataset('rewards', data=session.rewards)

    # units
    units_grp = f.create_group('units')
    for ttd, units in session.tetrode_unit_map.items():
        for unit in units:
            spike_idxs = []
            for sample in session.tetrodes[ttd][unit]:
                spike_idxs.append( np.abs(session.trajectory[:, -1] - sample).argmin() )

            unit_grp = units_grp.create_group('%s_%s' % (ttd, unit))

            # spiketrains are INDEXES of trajectory, not times or samples
            unit_grp.create_dataset('spike_idxs', data=np.array(spike_idxs))

    # velocity
    f.create_dataset('velocity', data=session.smoothed_velocity())

    # head direction
    f.create_dataset('head_direction', data=session.head_direction())

    # running indexes
    f.create_dataset('running_idxs', data=session.get_running_indexes())

    # settings
    f.attrs['settings'] = str(session.settings)

    f.close()
