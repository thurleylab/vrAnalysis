import numpy as np
import time
from datetime import datetime


def load_events(filepath):
    """

    :param filepath:
    :return:            frequency (int)
                        events - an array of events shape <num_rows> * 4
                          1 - proc time in samples, starting from beginning of recording
                          2 - outbound time of the remote event in sec since January 1, 1970
                          3 - X position as float
                          4 - Y position as float

    """
    with open(filepath) as f:
        raw = [x.split(' ') for x in f.readlines()]

    for i, line in enumerate(raw):
        if "Processor:" in line:
            processor_line = i
            specs = line[-1].split('@')
            t_start = int(specs[0])
            frequency = int(specs[1].replace('Hz\x00\n', ''))

            break
    else:
        raise ValueError("Processor specs (frequency etc.) not found")

    def to_timestamp(time_as_str):
        dt = datetime.strptime(time_as_str, '%Y-%m-%d %H:%M:%S.%f')
        return time.mktime(dt.timetuple()) + dt.microsecond * 10**-6

    # FIXME parse different formats - make real adapters and pass them to constructor

    # version 0.1
    # events = np.array([(
    #            int(x[0]) - t_start,
    #            to_timestamp("%s %s" % (x[2], x[3])),
    #            np.float64(x[5]),
    #            np.float64(x[7].replace('\x00\n', ''))
    #        ) for x in raw[processor_line + 1:-1]])

    # version 0.2
    events = np.array([(
               int(x[0]) - t_start,
               float(x[2]),
               np.float64(x[4]),
               np.float64(x[8].replace('\x00\n', ''))
           ) for x in raw[processor_line + 2:-1]])

    return frequency, events