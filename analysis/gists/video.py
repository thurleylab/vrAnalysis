

# CUT with ffmpeg
# from https://stackoverflow.com/questions/18444194/cutting-the-videos-based-on-start-and-end-time-using-ffmpeg
# ffmpeg -i input.avi -ss 00:02:00 -t 00:01:00 -async 1 output.mp4


# ROTATE with ffmpeg
# from https://stackoverflow.com/questions/3937387/rotating-videos-with-ffmpeg
# ffmpeg -i in.mov -vf "transpose=1" out.mov