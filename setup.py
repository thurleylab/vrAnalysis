from setuptools import setup, find_packages
import os


__author__ = 'asobolev'


setup(
    name='analysis',
    version='0.1',
    description='Analysis scripts for recording sessions (Place Cell activity etc.)',
    author='Andrey Sobolev',
    author_email='sobolev@bio.lmu.de',
    packages=find_packages(),
    include_package_data=True,
    package_data={'': [os.path.join('notebooks', '*.ipynb')]},
    install_requires=['numpy']
)
